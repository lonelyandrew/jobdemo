# 课设项目代码仓库Commit说明 #

** 2014-06-15 10：00 初始化仓库**

目前已经包含一个完整的增删改查实例，即专业信息的全套流程，但是由于体量较小，所以写在了一个页面，如有需要，可以分页来写。前台管理员，毕业生，用人单位的注册登录页html页已经写完，除了数据绑定不要修改任何部分的ID,Name,Class,如果要新建控件，请务必清晰命名。  

** 2014-06-15 20：00 后台布局完成**

后台页面的布局和html部分已经完全完成，就差数据绑定了，肯定还有较多bug，但是我一个人debug肯定不行，无论你们会不会做都得赶紧做了，我能帮的只有这些了。**有bug及时反馈。**