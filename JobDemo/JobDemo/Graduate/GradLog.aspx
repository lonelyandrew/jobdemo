﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GradLog.aspx.cs" Inherits="JobDemo.Graduate.GradLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>毕业生登录</title>
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="../DatePicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../Style/Graduate.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../DatePicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../DatePicker/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="../Scripts/Graduate/GradLog.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">毕业生登录</span>
        </div>
    </nav>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <div class="form-group">
            <label for="GradLogUsrName" class="col-sm-3 control-label">用户名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="LogGradUsrName" placeholder="用户名" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <label for="GrudLogPsd" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="LogGrudPsd" placeholder="密码" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <a class="btn btn-success compregbtn" runat="server" id="GrudLogSubmit" onserverclick="GrudLogSubmitEvent">
                <span class="glyphicon glyphicon-send"></span>&nbsp;登录
            </a>
            <a class="btn btn-danger compregbtn" id="GradrLogCancel">
                <span class="glyphicon glyphicon-home"></span>&nbsp;返回首页
            </a>
        </div>
    </form>
</body>
</html>
