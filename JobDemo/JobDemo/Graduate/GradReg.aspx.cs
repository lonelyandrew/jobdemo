﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tools;
using Model;
using BLL;
using System.Text;
using System.Data;

namespace JobDemo.Graduate
{
    public partial class GradReg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MajorBind();
                if (Session["Id"] != null)
                {
                    if (Session["Role"].ToString() == "Graduate")
                    {
                        BindData();
                    }
                }
            }
        }

        /// <summary>
        /// 编辑毕业生信息时，获取信息
        /// </summary>
        private void BindData()
        {
            DataTable Table = new DataTable();
            string StringSession = Session["Id"].ToString();
            Table = BLLGraduate.GetGraduateInfo(StringSession);
            if (Table.Rows.Count > 0)
            {
                DataRow Row = Table.Rows[0];
                AddGradName.Value = Row["Name"].ToString().Trim();
                AddGradUsrName.Value = Row["UserName"].ToString().Trim();
                AddGradSchool.Value = Row["School"].ToString().Trim();
                AddGrudBirthday.Value = Row["Birthday"].ToString().Trim();
                AddGrudSite.Value = Row["Location"].ToString().Trim();
                AddGrudTime.Value = Row["Time"].ToString().Trim();
                MajorSort.Value = Row["MajorId"].ToString().Trim();
                if (Row["Sex"].ToString() == "True")
                {
                    AddGradSex.Checked = true;
                }
                else
                {
                    AddGradSex.Checked = false;
                }
            }
        }

        protected void GrudRegSubmitEvent(Object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(AddGradName.Value))
            {
                ScriptHelper.Alert("姓名不能为空！", this.Page);
                return;
            }
            if (AddGradName.Value.Length > 5)
            {
                ScriptHelper.Alert("姓名长度超出规定!", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(AddGradUsrName.Value))
            {
                ScriptHelper.Alert("用户名不能为空！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(AddGradSchool.Value))
            {
                ScriptHelper.Alert("毕业院校不能为空！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(AddGrudSite.Value))
            {
                ScriptHelper.Alert("当前所在地不能为空！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(AddGrudTime.Value))
            {
                ScriptHelper.Alert("毕业时间不能为空！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(AddGrudBirthday.Value))
            {
                ScriptHelper.Alert("生日不能为空！", this.Page);
                return;
            }

            if (Session["Id"] == null && BLLGraduate.ContainsGradUserName(AddGradUsrName.Value))
            {
                ScriptHelper.Alert("此用户名已存在，请重新输入！", this.Page);
                return;
            }
            else if (Session["Id"] != null)
            {
                DataTable Table = BLLGraduate.GetGraduateInfo(Session["Id"].ToString());
                if (Table.Rows[0]["UserName"].ToString() != AddGradUsrName.Value)
                {
                    if (BLLGraduate.ContainsGradUserName(AddGradUsrName.Value.Trim()))
                    {
                        ScriptHelper.Alert("此用户名已存在，请重新输入！", this.Page);
                        return;
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(AddGrudPsd.Value))
            {
                ScriptHelper.Alert("密码不能为空！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(AddGrudRePsd.Value))
            {
                ScriptHelper.Alert("重复密码不能为空！", this.Page);
                return;
            }

            if (AddGrudPsd.Value != AddGrudRePsd.Value)
            {
                ScriptHelper.Alert("密码不一致，请重新输入！", this.Page);
                return;
            }

            Model.Graduate Model = new Model.Graduate();
            Model.Name = AddGradName.Value.Trim();
            Model.UserName = AddGradUsrName.Value.Trim();
            Model.Password = AddGrudPsd.Value.Trim();
            Model.Sex = AddGradSex.Checked.ToString();
            Model.Birthday = AddGrudBirthday.Value.Trim();
            Model.Location = AddGrudSite.Value.Trim();
            Model.Time = AddGrudTime.Value.Trim();
            Model.MajorId = MajorSort.Value.Trim();
            Model.School = AddGradSchool.Value.Trim();


            if (Session["Id"] == null)
            {
                if (BLLGraduate.AddGraduate(Model))
                {
                    Response.Redirect("Gradlog.aspx");
                }
                else
                {
                    ScriptHelper.Alert("注册失败，请联系管理员。", this.Page);
                    return;
                }
            }
            else
            {
                Model.Id = Int32.Parse(Session["Id"].ToString());
                if (BLLGraduate.UpdateGraduate(Model))
                {
                    Response.Redirect("../RecList.aspx");
                }
                else
                {
                    ScriptHelper.Alert("修改失败,请联系管理员！", this.Page);
                    return;
                }
            }
        }

        private void MajorBind()
        {
            MajorSort.DataValueField = "Id";
            MajorSort.DataTextField = "Name";
            MajorSort.DataSource = BLLMajor.GetAllMajorInfo();
            MajorSort.DataBind();
        }
    }
}