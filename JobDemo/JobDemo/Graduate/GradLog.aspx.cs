﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tools;
using Model;
using BLL;

namespace JobDemo.Graduate
{
    public partial class GradLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }

        }

        protected void GrudLogSubmitEvent(Object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(LogGradUsrName.Value))
            {
                ScriptHelper.Alert("请输入用户名！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(LogGrudPsd.Value))
            {
                ScriptHelper.Alert("密码不能为空！", this.Page);
                return;
            }

            Model.Graduate model = new Model.Graduate();
            model.UserName = LogGradUsrName.Value.Trim();
            model.Password = LogGrudPsd.Value.Trim();

            Model.Graduate.JudgeGraduateLog Result = BLLGraduate.GraduateLog(model);
            if (Result == Model.Graduate.JudgeGraduateLog.CompUserNameNotExist)
            {
                ScriptHelper.Alert("用户名不存在，请重新输入！", this.Page);
                return;
            }

            else if (Result == Model.Graduate.JudgeGraduateLog.PasswordWrong)
            {
                ScriptHelper.Alert("密码输入错误，请重新输入!", this.Page);
                return;
            }

            else
            {
                Session["Id"] = BLLGraduate.GetGraduateId(model);
                Session["Role"] = "Graduate";
                Response.Redirect("../RecList.aspx");
            }


        }
    }
}