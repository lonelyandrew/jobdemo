﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JobDemo.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>大学生就业咨询系统</title>
    <link href="BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="Style/Default.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="BootStrap/js/bootstrap.js"></script>
    <!-- Customed css & js -->
    <script src="Scripts/Front/Default.js"></script>
</head>
<body>

    <div class="navbar-wrapper">
        <nav class="navbar navbar-default " role="navigation">
            <div class="navbar-header">
                <span class="navbar-brand">大学生就业咨询系统(Pre-alpha)&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-send"></span></span>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <form class="navbar-form navbar-left" role="search">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="FirstLi">专业信息</span><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <asp:Repeater runat="server" ID="MajorSort">
                                            <ItemTemplate>
                                                <li><a name="MajorName" href="#" value='<%#Eval("Id") %>'><%#Eval("Name") %> </a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li class="divider"></li>
                                        <li><a name="MajorName" href="#">专业信息</a></li>
                                    </ul>
                                </div>
                                <!-- /btn-group -->
                                <input type="text" class="form-control" placeholder="Search" id="SearchBox" style="width: 170px;" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" id="SearchBtn"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a name="RegisBtn" href="#">注册</a></li>
                    <li><a href="Company/CompanyLogIn.aspx">企业登录</a></li>
                    <li><a href="Graduate/GradLog.aspx">毕业生登录</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div id="picover">
        <div id="textover">
            <div class="jumbotron">
                <h1 id="my-shine-object">未来，就在现在!</h1>
                <p>——&nbsp;&nbsp;抓住所有可以抓住的机会，成就所有可以成就的事业。</p>
                <p>
                    <a name="RegisBtn" class="btn btn-primary btn-lg" role="button">Join Us</a>
                    <a name="StaBtn" class="btn btn-info btn-lg" role="button" href="Statistics.aspx">查看历年统计数据</a>

                </p>
            </div>
        </div>
        <img id="defpic" src="Img/def.jpg" id="largepic" />
    </div>


    <!--Model part -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog" id="kkk">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">注册账号，开启明天</h4>
                </div>
                <div class="modal-body">
                    <div class="jumbotron">
                        <h1>"开启全新人生."</h1>
                        <p>——&nbsp;&nbsp;加入我们，与千万求职者挑战自己。</p>
                    </div>
                    <a href="Graduate/GradReg.aspx" class="btn btn-success">求职注册入口</a>
                    <a href="Company/CompanyRegister.aspx" class="btn btn-warning">招聘注册入口</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
