﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobDemo
{
    public partial class Statistics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Id"] == null || Session["Role"] == null)
            {
                RegBar.Visible = true;
            }
            else
            {
                ListBar.Visible = true;
            }

            if (!IsPostBack)
            {
                JsonCompBind();
                BindHot();
                BindCold();
                BindYear();

                if (Session["Id"] != null)
                {
                    SetProfile();
                }
            }
        }

        private void SetProfile()
        {
            DataRow Row = null;
            if (Session["Role"].ToString() == "Admin")
            {
                Model.Admin model = new Model.Admin();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLAdmin.GetInfoById(model);
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["UserName"] + "&nbsp;&nbsp;登录角色：管理员";
                EditProfile.HRef = "Admin/AdminReg.aspx";
            }
            else if (Session["Role"].ToString() == "Company")
            {
                Model.Company model = new Model.Company();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLCompany.GetInfoById(model).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：用人单位";
                EditProfile.HRef = "Company/CompanyRegister.aspx";
            }
            else if (Session["Role"].ToString() == "Graduate")
            {
                Model.Company model = new Model.Company();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLCompany.GetInfoById(model).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：毕业生";
                EditProfile.HRef = "Graduate/GradReg.aspx";
            }
            else
            {
                Response.Redirect("../Default.aspx");
            }
        }

        protected void LogOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/Default.aspx");
        }

        private void JsonCompBind()
        {
            StringBuilder JsonStr = new StringBuilder();
            JsonStr.Append("{ \"labels\":[");
            DataTable Table = BLLRec.GetTop5PeopleCountList();
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append("\"" + Item["Name"].ToString() + "\",");
                }
                else
                {
                    JsonStr.Append("\"" + Item["Name"].ToString() + "\"");
                }
            }
            JsonStr.Append("],\"datasets\": [{ \"fillColor\":\"rgba(0,255,255,0.5)\", \"strokeColor\": \"rgba(0,0,0,1)\", \"data\": [");
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append(Item["CompReq"].ToString() + ",");
                }
                else
                {
                    JsonStr.Append(Item["CompReq"].ToString());
                }
            }

            JsonStr.Append("] }] }");
            //JsonStr.Append("{ \"labels\":[\"    公司一\", \"公司二\", \"公司三\", \"公司四\", \"公司五\"      ],\"datasets\": [{ \"fillColor\":\"rgba(220,0,0,0.5)\", \"strokeColor\": \"rgba(220,220,220,1)\", \"data\": [65, 59, 90, 81, 56] }] }");
            JsonComp.Value = JsonStr.ToString();
        }

        private void BindHot()
        {
            StringBuilder JsonStr = new StringBuilder();
            JsonStr.Append("{ \"labels\":[");
            DataTable Table = BLLRec.GetTop3PeopleCountList();
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append("\"" + Item["MajorName"].ToString() + "\",");
                }
                else
                {
                    JsonStr.Append("\"" + Item["MajorName"].ToString() + "\"");
                }
            }
            JsonStr.Append("],\"datasets\": [{ \"fillColor\":\"rgba(255,0,0,0.5)\", \"strokeColor\": \"rgba(0,0,0,1)\", \"data\": [");
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append(Item["MajorPeopleCount"].ToString() + ",");
                }
                else
                {
                    JsonStr.Append(Item["MajorPeopleCount"].ToString());
                }
            }

            JsonStr.Append("] }] }");
            //JsonStr.Append("{ \"labels\":[\"    公司一\", \"公司二\", \"公司三\", \"公司四\", \"公司五\"      ],\"datasets\": [{ \"fillColor\":\"rgba(220,0,0,0.5)\", \"strokeColor\": \"rgba(220,220,220,1)\", \"data\": [65, 59, 90, 81, 56] }] }");
            JsonHot3.Value = JsonStr.ToString();
        }

        private void BindCold()
        {
            StringBuilder JsonStr = new StringBuilder();
            JsonStr.Append("{ \"labels\":[");
            DataTable Table = BLLRec.GetBottom3PeopleCountList();
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append("\"" + Item["MajorName"].ToString() + "\",");
                }
                else
                {
                    JsonStr.Append("\"" + Item["MajorName"].ToString() + "\"");
                }
            }
            JsonStr.Append("],\"datasets\": [{ \"fillColor\":\"rgba(0,0,255,0.5)\", \"strokeColor\": \"rgba(0,0,0,1)\", \"data\": [");
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append(Item["MajorPeopleCount"].ToString() + ",");
                }
                else
                {
                    JsonStr.Append(Item["MajorPeopleCount"].ToString());
                }
            }

            JsonStr.Append("] }] }");
            //JsonStr.Append("{ \"labels\":[\"    公司一\", \"公司二\", \"公司三\", \"公司四\", \"公司五\"      ],\"datasets\": [{ \"fillColor\":\"rgba(220,0,0,0.5)\", \"strokeColor\": \"rgba(220,220,220,1)\", \"data\": [65, 59, 90, 81, 56] }] }");
            JsonCold3.Value = JsonStr.ToString();
        }

        private void BindYear()
        {
            StringBuilder JsonStr = new StringBuilder();
            JsonStr.Append("{ \"labels\":[");
            DataTable Table = BLLRec.GetYearSumPeopleCountList();
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append("\"" + Item["YearTime"].ToString() + "\",");
                }
                else
                {
                    JsonStr.Append("\"" + Item["YearTime"].ToString() + "\"");
                }
            }
            JsonStr.Append("],\"datasets\": [{ \"fillColor\":\"rgba(255,255,0,0.5)\", \"strokeColor\": \"rgba(0,0,0,1)\", \"data\": [");
            foreach (DataRow Item in Table.Rows)
            {
                if (Table.Rows.IndexOf(Item) < (Table.Rows.Count - 1))
                {
                    JsonStr.Append(Item["MaxCount"].ToString() + ",");
                }
                else
                {
                    JsonStr.Append(Item["MaxCount"].ToString());
                }
            }

            JsonStr.Append("] }] }");
            //JsonStr.Append("{ \"labels\":[\"    公司一\", \"公司二\", \"公司三\", \"公司四\", \"公司五\"      ],\"datasets\": [{ \"fillColor\":\"rgba(220,0,0,0.5)\", \"strokeColor\": \"rgba(220,220,220,1)\", \"data\": [65, 59, 90, 81, 56] }] }");
            Json5Year.Value = JsonStr.ToString();
        }


    }
}