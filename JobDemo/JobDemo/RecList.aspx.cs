﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobDemo
{
    public partial class RecList : System.Web.UI.Page
    {

        private static int PageSize = 10;

        private static PagedDataSource DataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Id"] == null || Session["Role"] == null)
            {
                RegBar.Visible = true;
            }
            else
            {
                ListBar.Visible = true;
            }
            if (!IsPostBack)
            {
                MajorBind();
                RecListBind();
                if (Session["Id"] != null)
                {
                    SetProfile();
                }
            }
        }

        private void MajorBind()
        {
            MajorSort.DataSource = BLLMajor.GetAllMajorInfo();
            MajorSort.DataBind();
        }


        private void SetProfile()
        {
            DataRow Row = null;
            if (Session["Role"].ToString() == "Admin")
            {
                Model.Admin model = new Model.Admin();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLAdmin.GetInfoById(model);
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["UserName"] + "&nbsp;&nbsp;登录角色：管理员";
                EditProfile.HRef = "Admin/AdminReg.aspx";
            }
            else if (Session["Role"].ToString() == "Company")
            {
                Model.Company model = new Model.Company();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLCompany.GetInfoById(model).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：用人单位";
                EditProfile.HRef = "Company/CompanyRegister.aspx";
            }
            else if (Session["Role"].ToString() == "Graduate")
            {
                //Model.Graduate model = new Model.Graduate();
                //model.Id = Int32.Parse(Session["Id"].ToString());
                //Row = BLLCompany.GetInfoById(model).Rows[0];
                Row = BLLGraduate.GetGraduateInfo(Session["Id"].ToString()).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：毕业生";
                EditProfile.HRef = "Graduate/GradReg.aspx";
                Operation.Style.Add("visibility", "visible");
            }
            else
            {
                Response.Redirect("../Default.aspx");
            }
        }

        protected void LogOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/Default.aspx");
        }



        public void RecListBind()
        {
            DataSource = new PagedDataSource();
            DataSource.AllowPaging = true;
            DataSource.PageSize = PageSize;
            DataSource.CurrentPageIndex = 0;
            if (Request.QueryString["qw"] == null)
            {
                DataSource.DataSource = BLLRec.GetAllInfo().DefaultView;
            }
            else if (Request.QueryString["mi"] == null)
            {
                DataSource.DataSource = BLLRec.GetNeedList(Request.QueryString["qw"].ToString()).DefaultView;
            }
            else
            {
                DataSource.DataSource = BLLRec.GetNeedList(Request.QueryString["qw"].ToString(), Int32.Parse(Request.QueryString["mi"].ToString())).DefaultView;
            }
            ListRepeater.DataSource = DataSource;
            ListRepeater.DataBind();
            PagerButtonCheck();
        }

        private void PagerButtonCheck()
        {
            if (DataSource.CurrentPageIndex == (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = true;
                LastPageBtn.Disabled = true;
            }
            if (DataSource.CurrentPageIndex == 0)
            {
                PrePageBtn.Disabled = true;
                FirstPageBtn.Disabled = true;

            }
            if (DataSource.CurrentPageIndex > 0)
            {
                PrePageBtn.Disabled = false;
                FirstPageBtn.Disabled = false;
            }
            if (DataSource.CurrentPageIndex < (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = false;
                LastPageBtn.Disabled = false;
            }
        }

        protected void LastPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = DataSource.PageCount - 1;
            ListRepeater.DataSource = DataSource;
            ListRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void FirstPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = 0;
            ListRepeater.DataSource = DataSource;
            ListRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void NextPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex += 1;
            ListRepeater.DataSource = DataSource;
            ListRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void PrePage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex -= 1;
            ListRepeater.DataSource = DataSource;
            ListRepeater.DataBind();
            PagerButtonCheck();
        }
    }
}