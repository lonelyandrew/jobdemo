﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecDetail.aspx.cs" Inherits="JobDemo.RecDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>人才需求信息</title>
    <%--    <link href="Style/RecList.css" rel="stylesheet" />--%>
    <link href="Style/RecDetail.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="Sider/jquery.sidr.light.css" rel="stylesheet" />

    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="Sider/jquery.sidr.min.js"></script>
    <script src="Scripts/Front/RecDetail.js"></script>
</head>
<body>
    <form runat="server">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <span class="navbar-brand">人才需求信息详情&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"></span></span>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <div class="navbar-form navbar-left" role="search">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="FirstLi">专业信息</span><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <asp:Repeater runat="server" ID="MajorSort">
                                            <ItemTemplate>
                                                <li><a name="MajorName" href="#" value='<%#Eval("Id") %>'><%#Eval("Name") %> </a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li class="divider"></li>
                                        <li><a name="MajorName" href="#">专业信息</a></li>
                                    </ul>
                                </div>
                                <!-- /btn-group -->
                                <input type="text" class="form-control" placeholder="Search" id="SearchBox" style="width: 170px;" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" id="SearchBtn"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="nav navbar-nav navbar-right" id="RegBar" runat="server" visible="false">
                    <li><a name="RegisBtn" href="#">注册</a></li>
                    <li><a href="Company/CompanyLogIn.aspx">企业登录</a></li>
                    <li><a href="Graduate/GradLog.aspx">毕业生登录</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right" id="ListBar" runat="server" visible="false">
                    <li><a id="MyProfile" runat="server" class="btn btn-link" disabled="disabled"></a></li>
                    <li><a id="SiderBtn" href="#"><span class="glyphicon glyphicon-list listbtn"></span></a></li>
                </ul>

            </div>
        </nav>

        <div id="content">
            <asp:Repeater runat="server" ID="DetailContent">
                <ItemTemplate>
                    <div class="list-group">
                        <a class="list-group-item active"><%#Eval("Name")%></a>
                        <a class="list-group-item" href='<%# "CompDetail.aspx?Id="+Eval("CompanyId") %>'>招聘单位：<%#Eval("CompanyName")%></a>
                        <a class="list-group-item">招聘职位：<%#Eval("Position") %></a>
                        <a class="list-group-item">招聘人数：<%#Eval("Count") %></a>
                        <a class="list-group-item">专业限定：<%#Eval("MajorName") %></a>
                        <a class="list-group-item">工作地点：<%#Eval("Place") %></a>
                        <a class="list-group-item">专业限定：<%#Eval("MajorName") %></a>
                        <a class="list-group-item">发布时间：<%#Eval("Time") %></a>
                        <a class="list-group-item">薪资待遇：<%#Eval("Salary") %></a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <p>招聘详情：</p>
            <div runat="server" id="RecContent" class="well well-lg"></div>
        </div>


        <div id="SiderContent">
            <header id="demoheader">
                <h1>用户操作</h1>
            </header>
            <div id="demo-content">
                <div id="navigation">
                    <nav class="nav">
                        <ul>
                            <li><a id="EditProfile" runat="server" href="#">编辑个人资料&nbsp;&nbsp;&nbsp;<span name="editprofile"></span></a></li>
                            <li><a href="#" runat="server" id="LogOutBtn" onserverclick="LogOut">注销&nbsp;&nbsp;&nbsp;<span name="logoff"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="display: none;">
            <div class="modal-dialog" id="kkk">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">注册账号，开启明天</h4>
                    </div>
                    <div class="modal-body">
                        <div class="jumbotron">
                            <h1>"开启全新人生."</h1>
                            <p>——&nbsp;&nbsp;加入我们，与千万求职者挑战自己。</p>
                        </div>
                        <a href="Graduate/GradReg.aspx" class="btn btn-success">求职注册入口</a>
                        <a href="Company/CompanyRegister.aspx" class="btn btn-warning">招聘注册入口</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
