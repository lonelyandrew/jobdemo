﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tools;
using Model;
using BLL;

namespace JobDemo.Admin
{
    public partial class AdminLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void AdminLogSubmitEvent(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(LogAdminUsrName.Value))
            {
                ScriptHelper.Alert("用户名不能为空！", this.Page);
                return;
            }


            if (String.IsNullOrWhiteSpace(LogAdminPsd.Value))
            {
                ScriptHelper.Alert("密码不能为空！", this.Page);
                return;
            }

            Model.Admin model = new Model.Admin();
            model.UserName = LogAdminUsrName.Value.Trim();
            model.Password = LogAdminPsd.Value.Trim();
            Model.Admin.JudgeAdminLog Result = BLLAdmin.JudgeAdminlog(model);

            if (Result == Model.Admin.JudgeAdminLog.UserNameNotExist)
            {
                ScriptHelper.Alert("此账号不存在！", this.Page);
                return;
            }

            else if (Result == Model.Admin.JudgeAdminLog.WrongPassword)
            {
                ScriptHelper.Alert("密码错误，请重新输入！", this.Page);
                return;
            }
            else
            {
                Session["Id"] = BLLAdmin.GetAdminId(model);
                Session["Role"] = "Admin";
                Response.Redirect("MajorManagement.aspx");
            }

        }
    }
}