﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLog.aspx.cs" Inherits="JobDemo.Admin.AdminLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>管理员登录</title>
    <link href="../Style/Admin.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Scripts/Admin/AdminLog.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">管理员登录</span>
        </div>
    </nav>
    <form id="AdminLogInForm" runat="server" role="form" class="AdminForm form-horizontal adminform">
        <div class="form-group">
            <label for="AdminLogUsrName" class="col-sm-3 control-label">用户名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="LogAdminUsrName" placeholder="用户名" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label for="AdminLogPsd" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="LogAdminPsd" placeholder="密码" runat="server" />
            </div>
        </div>
        <div class="AdminLogBtn form-group">
            <a class="btn btn-success AdminLogbtn" runat="server" id="AdminLogSubmit" onserverclick="AdminLogSubmitEvent">
                <span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;&nbsp;登录
            </a>
            <a class="btn btn-danger AdminLogbtn" runat="server" id="AdminpregCancel" href="~/Default.aspx">
                <span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;返回首页
            </a>
        </div>
    </form>
</body>
</html>
