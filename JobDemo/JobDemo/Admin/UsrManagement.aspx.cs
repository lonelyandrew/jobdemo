﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Tools;

namespace JobDemo.Admin
{
    public partial class UsrManagement : System.Web.UI.Page
    {

        private static int PageSize = 7;

        private static PagedDataSource DataSource;

        private static Repeater CurrentRepeater;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Id"] == null)
            {
                Response.Redirect("../Default.aspx");
            }
            if (Session["Role"].ToString() != "Admin")
            {
                Response.Redirect("../Default.aspx");
            }
            if (!IsPostBack)
            {
                BindComp();
            }
        }

        protected void ViewChanged(object sender, EventArgs e)
        {
            HtmlAnchor Anchor = (HtmlAnchor)sender;
            switch (Anchor.InnerText)
            {
                case "用人单位": if (MultiViewUser.ActiveViewIndex != 0)
                    {
                        MultiViewUser.ActiveViewIndex = 0;
                        BindComp();
                    }; break;
                case "毕业生": if (MultiViewUser.ActiveViewIndex != 1)
                    {
                        MultiViewUser.ActiveViewIndex = 1;
                        BindGrad();
                    }; break;
                default: Response.Redirect("~/Default.aspx"); break;
            }
        }

        private void BindComp()
        {
            DataSource = new PagedDataSource();
            DataSource.AllowPaging = true;
            DataSource.PageSize = PageSize;
            DataSource.CurrentPageIndex = 0;
            DataSource.DataSource = BLLCompany.GetCompInfo().DefaultView;
            DataListComp.DataSource = DataSource;
            DataListComp.DataBind();
            CurrentRepeater = DataListComp;
            PagerButtonCheck();
            compnav.Attributes.Add("class", "navliactive");
            gradnav.Attributes.Remove("class");
        }

        private void BindGrad()
        {
            DataSource = new PagedDataSource();
            DataSource.AllowPaging = true;
            DataSource.PageSize = PageSize;
            DataSource.CurrentPageIndex = 0;
            DataSource.DataSource = BLLGraduate.GetGraduateInfo().DefaultView;
            DataListGrad.DataSource = DataSource;
            DataListGrad.DataBind();
            CurrentRepeater = DataListGrad;
            PagerButtonCheck();
            gradnav.Attributes.Add("class", "navliactive");
            compnav.Attributes.Remove("class");
        }

        private void PagerButtonCheck()
        {
            if (DataSource.CurrentPageIndex == (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = true;
                LastPageBtn.Disabled = true;
            }
            if (DataSource.CurrentPageIndex == 0)
            {
                PrePageBtn.Disabled = true;
                FirstPageBtn.Disabled = true;

            }
            if (DataSource.CurrentPageIndex > 0)
            {
                PrePageBtn.Disabled = false;
                FirstPageBtn.Disabled = false;
            }
            if (DataSource.CurrentPageIndex < (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = false;
                LastPageBtn.Disabled = false;
            }
        }

        protected void LastPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = DataSource.PageCount - 1;
            CurrentRepeater.DataSource = DataSource;
            CurrentRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void FirstPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = 0;
            CurrentRepeater.DataSource = DataSource;
            CurrentRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void NextPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex += 1;
            CurrentRepeater.DataSource = DataSource;
            CurrentRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void PrePage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex -= 1;
            CurrentRepeater.DataSource = DataSource;
            CurrentRepeater.DataBind();
            PagerButtonCheck();
        }

        protected void TrueDelBtn_ServerClick(object sender, EventArgs e)
        {
            if (IdSpace.Value == "" || SortSpace.Value == "")
            {
                return;
            }

            if (SortSpace.Value == "Comp")
            {
                Model.Company model = new Model.Company();
                model.Id = Int32.Parse(IdSpace.Value);
                if (BLLCompany.DeleteCompany(model))
                {
                    BindComp();
                }
                else
                {
                    ScriptHelper.Alert("删除失败.", this.Page);
                }
            }
            else if (SortSpace.Value == "Grad")
            {
                Model.Graduate model = new Model.Graduate();
                model.Id = Int32.Parse(IdSpace.Value);

                if (BLLGraduate.DeleteGraduate(model))
                {
                    BindGrad();
                }
                else
                {
                    ScriptHelper.Alert("删除失败.", this.Page);
                }
            }
        }
    }
}