﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MajorManagement.aspx.cs" Inherits="JobDemo.Admin.MajorManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>专业信息管理</title>
    <link href="../Style/Admin.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="../Sider/jquery.sidr.light.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Sider/jquery.sidr.min.js"></script>
    <script src="../Scripts/Admin/Admin.js"></script>
</head>
<body>
    <form id="FormMajor" runat="server" role="form" class="form-horizontal">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <span class="navbar-brand">专业信息管理&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-cog"></span></span>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right" id="ListBar" runat="server">
                    <li><a id="MyProfile" runat="server" class="btn btn-link" disabled="disabled"></a></li>
                    <li><a id="SiderBtn" href="#"><span class="glyphicon glyphicon-list listbtn"></span></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-left ">
                    <li class="dropdown adminOperation">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">管理员操作<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="../RecList.aspx">人才需求查看</a></li>
                            <li class="divider"></li>
                            <li><a href="../Company/RecManagement.aspx">人才需求管理</a></li>
                            <li class="divider"></li>
                            <li><a href="../Statistics.aspx">历年统计数据</a></li>
                            <li class="divider"></li>
                            <li><a href="UsrManagement.aspx">用户管理</a></li>
                            <li class="divider"></li>
                            <li><a href="AdminReg.aspx?s=add">添加管理员</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <asp:MultiView ActiveViewIndex="0" runat="server" ID="MajorMultiView">
            <asp:View runat="server" ID="ListView">
                <div>
                    <table id="majortable" class="majortable table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" id="MajorSelectAll" />
                                </th>
                                <th>专业名称</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="DataListMajor">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <input type="checkbox" value="<%#Eval("Id") %>" name="MajorId" />

                                        </td>
                                        <td><%#Eval("Name") %></td>
                                        <td>
                                            <a name="editmajor" href="#" runat="server" onserverclick="EditMajorClick" class="btn btn-primary" value='<%#Eval("Id") %>'>
                                                <span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;&nbsp;编辑</a>
                                            <a name="delmajor" href="#" runat="server" onserverclick="DelMajorClick" value='<%#Eval("Id") %>' class="btn btn-danger">
                                                <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;&nbsp;删除</a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <div id="majorpagerdiv">
                    <span>第<span id="CurrentPageNum" runat="server"></span>页&nbsp;&nbsp;|&nbsp;&nbsp;共<span id="TotalPageNum" runat="server"></span>页</span>
                    <a class="btn btn-default majorpager" runat="server" onserverclick="FirstPage" id="FirstPageBtn">
                        <span class="glyphicon glyphicon-fast-backward"></span>&nbsp;&nbsp;&nbsp;首页
                    </a>
                    <a class="btn btn-default majorpager" runat="server" id="PrePageBtn" onserverclick="PrePage">
                        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp;&nbsp;&nbsp;上一页
                    </a>
                    <a class="btn btn-default majorpager" runat="server" id="NextPageBtn" onserverclick="NextPage">
                        <span class="glyphicon glyphicon-chevron-right"></span>&nbsp;&nbsp;&nbsp;下一页
                    </a>
                    <a class="btn btn-default majorpager" runat="server" onserverclick="LastPage" id="LastPageBtn">
                        <span class="glyphicon glyphicon-fast-forward"></span>&nbsp;&nbsp;&nbsp;尾页
                    </a>
                </div>
                <div id="majorlistbtn">
                    <a id="MultiDelBtn" runat="server" class="btn btn-danger" onserverclick="MultiDelMajorClick"><span class="glyphicon glyphicon-remove-circle">&nbsp;</span>删除选中项</a>
                    <a id="CreateBtn" runat="server" class="btn btn-primary" onserverclick="CreateBtnClick"><span class="glyphicon glyphicon-plus">&nbsp;</span>添加专业</a>
                </div>
            </asp:View>
            <asp:View runat="server" ID="CreateView">
                <div class="form-group" id="CreateMajorInput">
                    <label for="AddMajorName" class="col-sm-1 control-label">专业名称</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="AddMajorName" placeholder="专业名称" runat="server" />
                    </div>
                </div>
                <br />
                <div id="CreateButton">
                    <a id="AddMajorSubmit" runat="server" class="btn btn-info" href="#" onserverclick="EditMajorSubmit">提交</a>
                    <a id="CancelMajorEdit" runat="server" class="btn btn-warning" href="#" onserverclick="CancelMajorEditClick">取消</a>
                    <input type="hidden" id="IdHidden" runat="server" />
                </div>
            </asp:View>
        </asp:MultiView>
        <div id="SiderContent">
            <header id="demoheader">
                <h1>用户操作</h1>
            </header>
            <div id="demo-content">
                <div id="navigation">
                    <nav class="nav">
                        <ul>
                            <li><a href="AdminReg.aspx">编辑个人资料&nbsp;&nbsp;&nbsp;<span name="editprofile"></span></a></li>
                            <li><a href="#" runat="server" id="LogOutBtn" onserverclick="LogOut">注销&nbsp;&nbsp;&nbsp;<span name="logoff"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
