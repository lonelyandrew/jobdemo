﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataManagement.aspx.cs" Inherits="JobDemo.Admin.DataManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>数据管理</title>
    <link href="../Style/Admin.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Scripts/Admin/Admin.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">数据管理</span>
        </div>
    </nav>
    <form id="form1" runat="server">
        <div class="panel panel-primary databackup">
            <div class="panel-heading">
                <h3 class="panel-title">数据备份</h3>
            </div>
            <div class="panel-body">
                <a class="btn btn-warning">数据备份</a>
            </div>
        </div>
        <div class="panel panel-success databackup">
            <div class="panel-heading">
                <h3 class="panel-title">数据恢复</h3>
            </div>
            <div class="panel-body">
                <div class="input-group">
                    <input id="lefile" type="file" style="display: none" />
                    <input type="text" class="form-control" id="UpFilePath" />
                    <span class="input-group-btn">
                        <button id="UploadBtn" class="btn btn-default" type="button">上传备份</button>
                    </span>
                </div>
                <br />
                <a class="btn btn-danger">数据恢复</a>
            </div>
        </div>
    </form>
</body>
</html>
