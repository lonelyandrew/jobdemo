﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tools;
using Model;
using BLL;
using System.Text;
using System.Data;


namespace JobDemo.Admin
{
    public partial class AdminReg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["s"] == null)
                {
                    BindData();
                }
            }
        }

        private void BindData()
        {
            Model.Admin model = new Model.Admin();
            model.Id = Int32.Parse(Session["Id"].ToString());
            DataRow Row = BLLAdmin.GetInfoById(model);
            if (Row != null)
            {
                AddAdminName.Value = Row["UserName"].ToString().Trim();
            }
        }

        protected void AdminRegSubmitEvent(Object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(AddAdminName.Value))
            {
                ScriptHelper.Alert("用户名不能为空！", this.Page);
                return;
            }

            if (Session["Id"] == null && BLLAdmin.ContainsAdminUserName(AddAdminName.Value))
            {
                ScriptHelper.Alert("用户名已被注册！", this.Page);
                return;
            }
            else if (Session["Id"] != null)
            {
                using (Model.Admin model = new Model.Admin())
                {
                    model.Id = Int32.Parse(Session["Id"].ToString());
                    DataRow Row = BLLAdmin.GetInfoById(model);
                    if (Row["UserName"].ToString() != AddAdminName.Value)
                    {
                        if (BLLAdmin.ContainsAdminUserName(AddAdminName.Value))
                        {
                            ScriptHelper.Alert("用户名已被注册！", this.Page);
                            return;
                        }
                    }
                }
            }
            if (String.IsNullOrWhiteSpace(AddAdminPsd.Value))
            {
                ScriptHelper.Alert("密码不能为空！", this.Page);
                return;
            }

            Model.Admin Model = new Model.Admin();
            Model.UserName = AddAdminName.Value.Trim();
            Model.Password = AddAdminPsd.Value.Trim();

            if (Session["Id"] == null)
            {
                if (BLLAdmin.AddAdmin(Model))
                {
                    Response.Redirect("AdminLog.aspx");
                }
                else
                {
                    ScriptHelper.Alert("注册失败！", this.Page);
                }
            }
            else
            {
                Model.Id = Int32.Parse(Session["Id"].ToString());
                if (BLLAdmin.UpdateAdmin(Model))
                {
                    Response.Redirect("AdminLog.aspx");
                }
                else
                {
                    ScriptHelper.Alert("修改失败！", this.Page);
                }
            }

        }
    }
}