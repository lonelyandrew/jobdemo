﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsrManagement.aspx.cs" Inherits="JobDemo.Admin.UsrManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用户信息管理</title>
    <link href="../Style/Admin.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="../Sider/jquery.sidr.light.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Sider/jquery.sidr.min.js"></script>
    <script src="../Scripts/Admin/Admin.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <span class="navbar-brand">用户信息管理</span>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li id="compnav" class="navliactive" runat="server"><a href="#" runat="server" onserverclick="ViewChanged">用人单位</a></li>
                    <li id="gradnav" runat="server"><a href="#" runat="server" onserverclick="ViewChanged">毕业生</a></li>
                </ul>
            </div>
        </nav>
        <div id="hiddencontent">
            <asp:MultiView ActiveViewIndex="0" runat="server" ID="MultiViewUser">
                <asp:View runat="server">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>单位名称</th>
                                <th>所在地</th>
                                <th>联系电话</th>
                                <th>电子邮件</th>
                                <th>详情</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="DataListComp">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("Name").ToString().Length >20 ? Eval("Name").ToString().Substring(0,20): Eval("Name") %></td>
                                        <td><%#Eval("Site") %></td>
                                        <td><%#Eval("Contact") %></td>
                                        <td><%#Eval("Email") %></td>
                                        <td><a href='<%# "../CompDetail.aspx?Id="+Eval("Id") %>'>详情页</a></td>
                                        <td><a name="deloperation" value="<%#Eval("Id") %>" sort="Comp" class="btn btn-danger">删除<span class="glyphicon glyphicon-remove"></span></a></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </asp:View>
                <asp:View runat="server">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>姓名</th>
                                <th>用户名</th>
                                <th>性别</th>
                                <th>当前所在地</th>
                                <th>毕业时间</th>
                                <th>毕业院校</th>
                                <th>专业</th>
                                <th>出生日期</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="DataListGrad">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("Name")%></td>
                                        <td><%#Eval("UserName") %></td>
                                        <td><%#Eval("Sex") %></td>
                                        <td><%#Eval("Location") %></td>
                                        <td><%#Eval("Time") %></td>
                                        <td><%#Eval("School") %></td>
                                        <td><%#Eval("MajorName") %></td>
                                        <td><%#Eval("Birthday") %></td>
                                        <td><a name="deloperation" value='<%#Eval("Id") %>' sort="Grad" class="btn btn-danger">删除<span class="glyphicon glyphicon-remove"></span></a></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </asp:View>
                <asp:View runat="server">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>用户名</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="Repeater2">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("UserName") %></td>
                                        <td><a name="deloperation"><span class="glyphicon glyphicon-remove"></span></a></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </asp:View>
            </asp:MultiView>
            <div id="PagerContainer">
                <ul class="pager">
                    <li class="previous">
                        <a class="btn btn-default majorpager" runat="server" id="FirstPageBtn" onserverclick="FirstPage">
                            <span class="glyphicon glyphicon-fast-backward"></span>&nbsp;&nbsp;&nbsp;首页
                        </a>
                    </li>
                    <li class="previous">
                        <a class="btn btn-default majorpager" runat="server" id="PrePageBtn" onserverclick="PrePage">
                            <span class="glyphicon glyphicon-chevron-left"></span>&nbsp;&nbsp;&nbsp;上一页
                        </a>
                    </li>

                    <li class="next">
                        <a class="btn btn-default majorpager" runat="server" id="LastPageBtn" onserverclick="LastPage">尾页&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-fast-forward"></span>
                        </a>
                    </li>

                    <li class="next">
                        <a class="btn btn-default majorpager" runat="server" id="NextPageBtn" onserverclick="NextPage">下一页&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div id="myDelModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="display: none;">
            <div class="modal-dialog" id="kkk">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">删除用户</h4>
                    </div>
                    <div class="modal-body">
                        <div class="jumbotron">
                            <h1>"确认删除?"</h1>
                            <p>——&nbsp;&nbsp;谨慎删除，删除后不可恢复。</p>
                        </div>
                        <button type="button" class="btn btn-success" data-dismiss="modal">取消删除</button>
                        <button id="TrueDelBtn" type="button" class="btn btn-danger" data-dismiss="modal" runat="server" onserverclick="TrueDelBtn_ServerClick">确认删除</button>
                    </div>
                </div>
            </div>
        </div>
        <input id="IdSpace" type="hidden" runat="server" />
        <input id="SortSpace" type="hidden" runat="server" />
    </form>
</body>
</html>
