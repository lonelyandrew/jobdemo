﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecList.aspx.cs" Inherits="JobDemo.RecList" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>人才需求信息</title>
    <link href="Style/RecList.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="Sider/jquery.sidr.light.css" rel="stylesheet" />

    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="Sider/jquery.sidr.min.js"></script>
    <script src="Scripts/Front/RecList.js"></script>
</head>
<body>
    <form runat="server">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <span class="navbar-brand">人才需求信息&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"></span></span>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span id="FirstLi">专业信息</span><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <asp:Repeater runat="server" ID="MajorSort">
                                            <ItemTemplate>
                                                <li><a name="MajorName" href="#" value='<%#Eval("Id") %>'><%#Eval("Name") %> </a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li class="divider"></li>
                                        <li><a name="MajorName" href="#">专业信息</a></li>
                                    </ul>
                                </div>
                                <!-- /btn-group -->
                                <input type="text" class="form-control" placeholder="Search" id="SearchBox" style="width: 170px;" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" id="SearchBtn"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>


                <ul class="nav navbar-nav navbar-right" id="RegBar" runat="server" visible="false">
                    <li><a name="RegisBtn" href="#">注册</a></li>
                    <li><a href="Company/CompanyLogIn.aspx">企业登录</a></li>
                    <li><a href="Graduate/GradLog.aspx">毕业生登录</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right" id="ListBar" runat="server" visible="false">
                    <li><a id="MyProfile" runat="server" class="btn btn-link" disabled="disabled"></a></li>
                    <li><a id="SiderBtn" href="#"><span class="glyphicon glyphicon-list listbtn"></span></a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right" id="Operation" runat="server">
                    <li class="dropdown compOperation">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">毕业生操作<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="../RecList.aspx">人才需求查看</a></li>
                            <li class="divider"></li>
                            <li><a href="../Statistics.aspx">历年统计数据</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="content">
            <ol class="breadcrumb">
                <li><span>最新信息&nbsp;&nbsp;<span class="glyphicon glyphicon-time"></span></span></li>
            </ol>
            <div class="list-group">

                <asp:Repeater runat="server" ID="ListRepeater">
                    <ItemTemplate>
                        <a href='<%#"RecDetail.aspx?id="+Eval("Id")%>' class="list-group-item"><%# Eval("Name").ToString().Length>30?Eval("Name").ToString().Substring(0,30)+"...":Eval("Name") %><span class="badge"><%#Eval("Time") %></span></a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div id="PagerContainer">
            <ul class="pager">
                <li class="previous">
                    <a class="btn btn-default majorpager" runat="server" id="FirstPageBtn" onserverclick="FirstPage">
                        <span class="glyphicon glyphicon-fast-backward"></span>&nbsp;&nbsp;&nbsp;首页
                    </a>
                </li>
                <li class="previous">
                    <a class="btn btn-default majorpager" runat="server" id="PrePageBtn" onserverclick="PrePage">
                        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp;&nbsp;&nbsp;上一页
                    </a>
                </li>

                <li class="next">
                    <a class="btn btn-default majorpager" runat="server" id="LastPageBtn" onserverclick="LastPage">尾页&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-fast-forward"></span>
                    </a>
                </li>

                <li class="next">
                    <a class="btn btn-default majorpager" runat="server" id="NextPageBtn" onserverclick="NextPage">下一页&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </li>
            </ul>
        </div>

        <div id="SiderContent">
            <header id="demoheader">
                <h1>用户操作</h1>
            </header>
            <div id="demo-content">
                <div id="navigation">
                    <nav class="nav">
                        <ul>
                            <li><a id="EditProfile" href="#" runat="server">编辑个人资料&nbsp;&nbsp;&nbsp;<span name="editprofile"></span></a></li>
                            <li><a href="#" runat="server" id="LogOutBtn" onserverclick="LogOut">注销&nbsp;&nbsp;&nbsp;<span name="logoff"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="display: none;">
            <div class="modal-dialog" id="kkk">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">注册账号，开启明天</h4>
                    </div>
                    <div class="modal-body">
                        <div class="jumbotron">
                            <h1>"开启全新人生."</h1>
                            <p>——&nbsp;&nbsp;加入我们，与千万求职者挑战自己。</p>
                        </div>
                        <a href="Graduate/GradReg.aspx" class="btn btn-success">求职注册入口</a>
                        <a href="Company/CompanyRegister.aspx" class="btn btn-warning">招聘注册入口</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
