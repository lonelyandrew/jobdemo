﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyLogIn.aspx.cs" Inherits="JobDemo.Company.CompanyLogIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用人单位登录</title>
    <link href="../Style/Company.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />

    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Scripts/Company/CompLog.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">用人单位登录</span>
        </div>
    </nav>
    <form id="ComLogInForm" runat="server" role="form" class="ComForm form-horizontal">
        <div class="form-group">
            <label for="ComLogUsrName" class="col-sm-3 control-label">用户名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="LogCompUsrName" placeholder="用户名" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label for="ComLogPsd" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="LogCompPsd" placeholder="密码" runat="server" />
            </div>
        </div>
        <div class="CompLogBtn form-group">
            <a class="btn btn-success complogbtn" runat="server" id="CompLogSubmit" onserverclick="CompLogSbumitEvent">
                <span class="glyphicon glyphicon-send"></span>&nbsp;登录
            </a>
            <a class="btn btn-danger complogbtn" runat="server" id="CompregCancel">
                <span class="glyphicon glyphicon-home"></span>&nbsp;返回首页
            </a>
        </div>
    </form>
</body>
</html>
