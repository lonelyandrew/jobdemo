﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Tools;

namespace JobDemo.Company
{
    public partial class RecruitCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Id"] == null)
                {
                    Response.Redirect("CompanyLogIn.aspx");
                }
                MajorBind();

                if (Request.QueryString["Id"] != null)
                {
                    GetEditInfoById();
                }

                
            }
        }

        private void MajorBind()
        {
            MajorSort.DataValueField = "Id";
            MajorSort.DataTextField = "Name";
            MajorSort.DataSource = BLLMajor.GetAllMajorInfo();
            MajorSort.DataBind();
        }

        protected void RecCreateSubmit(object sender, EventArgs e)
        {
            Recruitment model = new Recruitment();

            if (String.IsNullOrWhiteSpace(AddRecTitle.Value.Trim()))
            {
                ScriptHelper.Alert("标题未填写。", this.Page);
                return;
            }
            else if (String.IsNullOrWhiteSpace(AddRecPlaces.Value.Trim()))
            {
                ScriptHelper.Alert("工作地点未填写。", this.Page);
                return;
            }
            else if (String.IsNullOrWhiteSpace(AddRecCount.Value.Trim()))
            {
                ScriptHelper.Alert("职位数量未填写。", this.Page);
                return;
            }
            else if (String.IsNullOrWhiteSpace(AddRecPosition.Value.Trim()))
            {
                ScriptHelper.Alert("招聘职位未填写。", this.Page);
                return;
            }
            else if (String.IsNullOrWhiteSpace(AddRecSalary.Value.Trim()))
            {
                ScriptHelper.Alert("薪资待遇未填写。", this.Page);
                return;
            }
            else
            {
                try
                {
                    model.Count = Int32.Parse(AddRecCount.Value.Trim());

                    if (model.Count < 1)
                    {
                        ScriptHelper.Alert("职位数量错误", this.Page);
                        return;
                    }
                }
                catch
                {
                    ScriptHelper.Alert("职位数量请填写数字", this.Page);
                    return;
                }
            }

            model.Name = AddRecTitle.Value.Trim();
            model.Place = AddRecPlaces.Value.Trim();
            model.Position = AddRecPosition.Value.Trim();
            model.MajorId = Int32.Parse(MajorSort.Value);
            model.CompanyId = Int32.Parse(Session["Id"].ToString());
            model.ReleaseTime = DateTime.Now.ToLongDateString();
            model.Timed = false;
            model.Time = DateTime.Now.ToLongDateString();
            model.Salary = AddRecSalary.Value.Trim();
            model.Content = Request["content"].ToString();

            if (Request.QueryString["Id"] == null)
            {

                if (BLLRec.AddRec(model))
                {
                    Response.Redirect("RecManagement.aspx");

                }
                else
                {
                    ScriptHelper.Alert("添加失败。", this.Page);
                    return;
                }
            }
            else
            {
                model.Id = Int32.Parse(Request.QueryString["Id"].ToString());
                if (BLLRec.UpdateRec(model))
                {
                    Response.Redirect("RecManagement.aspx");
                }
                else
                {
                    ScriptHelper.Alert("更新失败。", this.Page);
                    return;
                }
            }
        }

        private void GetEditInfoById()
        {
            Recruitment model = new Recruitment();
            model.Id = Int32.Parse(Request.QueryString["Id"].ToString());
            DataRow Row = BLLRec.GetSingleRecById(model);

            AddRecTitle.Value = Row["Name"].ToString();
            AddRecPosition.Value = Row["Position"].ToString();
            AddRecCount.Value = Row["Count"].ToString();
            AddRecPlaces.Value = Row["Place"].ToString();
            AddRecSalary.Value = Row["Salary"].ToString();
            ContentSpace.Value = Row["Content"].ToString();

            for (int i = 0; i < MajorSort.Items.Count; i++)
            {
                ListItem Control = MajorSort.Items[i];
                if (Control.Value == Row["MajorId"].ToString())
                {
                    MajorSort.SelectedIndex = i;
                    break;
                }
            }
        }
    }
}