﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Tools;

namespace JobDemo.Company
{
    public partial class RecManagement : System.Web.UI.Page
    {
        private static int PageSize = 2;

        private static PagedDataSource DataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Id"] == null || Session["Role"] == null)
                {
                    Response.Redirect("../Default.aspx");
                }
                else
                {
                    RecListBind();
                }

                SetProfile();
            }
        }

        //Role & Id
        public void RecListBind()
        {
            DataSource = new PagedDataSource();
            DataSource.AllowPaging = true;
            DataSource.PageSize = PageSize;
            DataSource.CurrentPageIndex = 0;
            if (Session["Role"].ToString() == "Admin")
            {
                DataSource.DataSource = BLLRec.GetAllInfo().DefaultView;
            }
            else if (Session["Role"].ToString() == "Company")
            {
                Recruitment model = new Recruitment();
                model.CompanyId = Int32.Parse(Session["Id"].ToString());
                DataSource.DataSource = BLLRec.GetRecByCompId(model).DefaultView;
            }
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void LastPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = DataSource.PageCount - 1;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void FirstPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = 0;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void NextPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex += 1;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void PrePage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex -= 1;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        private void PagerButtonCheck()
        {
            if (DataSource.CurrentPageIndex == (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = true;
                LastPageBtn.Disabled = true;
            }
            if (DataSource.CurrentPageIndex == 0)
            {
                PrePageBtn.Disabled = true;
                FirstPageBtn.Disabled = true;

            }
            if (DataSource.CurrentPageIndex > 0)
            {
                PrePageBtn.Disabled = false;
                FirstPageBtn.Disabled = false;
            }
            if (DataSource.CurrentPageIndex < (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = false;
                LastPageBtn.Disabled = false;
            }

            CurrentPageNum.InnerText = (DataSource.CurrentPageIndex + 1).ToString();
            TotalPageNum.InnerText = DataSource.PageCount.ToString();
        }

        protected void TrueDelBtn_ServerClick(object sender, EventArgs e)
        {
            try
            {
                int DelId = Int32.Parse(Idhidden.Value);
                Recruitment model = new Recruitment();
                model.Id = DelId;
                if (BLLRec.DelRec(model))
                {
                    RecListBind();
                }
                else
                {
                    ScriptHelper.Alert("删除失败", this.Page);
                    RecListBind();
                    return;
                }
            }
            catch
            {
                return;
            }
        }

        private void SetProfile()
        {
            DataRow Row = null;
            if (Session["Role"].ToString() == "Admin")
            {
                Model.Admin model = new Model.Admin();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLAdmin.GetInfoById(model);
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["UserName"] + "&nbsp;&nbsp;登录角色：管理员";
                EditProfile.HRef = "../Admin/AdminReg.aspx";
            }
            else if (Session["Role"].ToString() == "Company")
            {
                Model.Company model = new Model.Company();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLCompany.GetInfoById(model).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：用人单位";
                EditProfile.HRef = "CompanyRegister.aspx";
                Operation.Style.Add("visibility", "visible");
            }
            else
            {
                Response.Redirect("../Default.aspx");
            }
        }

        protected void LogOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("../Default.aspx");
        }
    }
}