﻿using System;
using Tools;
using BLL;



namespace JobDemo.Company
{
    public partial class CompanyLogIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }

        }

        protected void CompLogSbumitEvent(Object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(LogCompUsrName.Value))
            {
                ScriptHelper.Alert("账户名为空，请重新输入！", this.Page);
                return;
            }

            if (String.IsNullOrWhiteSpace(LogCompPsd.Value))
            {
                ScriptHelper.Alert("密码不能为空！", this.Page);
                return;
            }

            Model.Company model = new Model.Company();
            model.UserName = LogCompUsrName.Value.Trim();
            model.Password = LogCompPsd.Value.Trim();

            Model.Company.JudgeCompLog Result = BLLCompany.CompLogIn(model);
            if (Result == Model.Company.JudgeCompLog.CompUserNameNotExist)
            {
                ScriptHelper.Alert("用户名不存在！", this.Page);
                return;
            }
            else if (Result == Model.Company.JudgeCompLog.PasswordWrong)
            {
                ScriptHelper.Alert("密码错误！", this.Page);
                return;
            }
            else
            {
                Session["Id"] = BLLCompany.GetCompanyId(model);
                Session["Role"] = "Company";
                Response.Redirect("RecManagement.aspx");
            }
        }
    }
}