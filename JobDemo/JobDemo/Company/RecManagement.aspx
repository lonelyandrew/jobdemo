﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecManagement.aspx.cs" Inherits="JobDemo.Company.RecManagement" EnableEventValidation="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Style/Company.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="../Sider/jquery.sidr.light.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Sider/jquery.sidr.min.js"></script>
    <script src="../UEditor/ueditor.config.js"></script>
    <script src="../UEditor/ueditor.all.min.js"></script>
    <script src="../Scripts/Company/CompRec.js"></script>
</head>
<body>
    <form id="recmanaform" runat="server">

        <div id="hidespace">
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <span class="navbar-brand">人才招聘管理&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-briefcase"></span></span>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" id="Operation" runat="server">
                        <li class="dropdown compOperation">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">用人单位操作<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="../RecList.aspx">人才需求查看</a></li>
                                <li class="divider"></li>
                                <li><a href="../Company/RecManagement.aspx">人才需求管理</a></li>
                                <li class="divider"></li>
                                <li><a href="../Statistics.aspx">历年统计数据</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a id="MyProfile" runat="server" class="btn btn-link" disabled="disabled"></a></li>
                        <li><a id="SiderBtn" href="#"><span class="glyphicon glyphicon-list listbtn"></span></a></li>
                    </ul>
                </div>

            </nav>
            <table id="rectable" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>标题</th>
                        <th>发布时间</th>
                        <th>详情页</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="DataListMajor">
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("Name").ToString().Length >20 ? Eval("Name").ToString().Substring(0,20): Eval("Name") %></td>
                                <td><%#Eval("Time") %></td>
                                <td><a href='<%#"../RecDetail.aspx?Id="+Eval("Id")%>'>详情页</a></td>
                                <td>
                                    <a name="editrec" href='<%# "RecruitCreate.aspx?id="+Eval("Id")%>' runat="server" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;&nbsp;编辑</a>
                                    <a name="delrec" href="#" runat="server" value='<%#Eval("Id") %>' class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;&nbsp;删除</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div id="recpagerdiv">
                <span>第<span id="CurrentPageNum" runat="server"></span>页&nbsp;&nbsp;|&nbsp;&nbsp;共<span id="TotalPageNum" runat="server"></span>页</span>
                <a class="btn btn-default majorpager" runat="server" onserverclick="FirstPage" id="FirstPageBtn">
                    <span class="glyphicon glyphicon-fast-backward"></span>&nbsp;&nbsp;&nbsp;首页
                </a>
                <a class="btn btn-default majorpager" runat="server" id="PrePageBtn" onserverclick="PrePage">
                    <span class="glyphicon glyphicon-chevron-left"></span>&nbsp;&nbsp;&nbsp;上一页
                </a>
                <a class="btn btn-default majorpager" runat="server" id="NextPageBtn" onserverclick="NextPage">
                    <span class="glyphicon glyphicon-chevron-right"></span>&nbsp;&nbsp;&nbsp;下一页
                </a>
                <a class="btn btn-default majorpager" runat="server" onserverclick="LastPage" id="LastPageBtn">
                    <span class="glyphicon glyphicon-fast-forward"></span>&nbsp;&nbsp;&nbsp;尾页
                </a>
            </div>

            <a class="btn btn-success btn-lg btn-block" id="CreateRecBtn">新建招聘</a>

        </div>

        <!-- Modal part-->
        <div id="myDelModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="display: none;">
            <div class="modal-dialog" id="kkk">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">删除招聘</h4>
                    </div>
                    <div class="modal-body">
                        <div class="jumbotron">
                            <h1>"确认删除?"</h1>
                            <p>——&nbsp;&nbsp;谨慎删除，删除后不可恢复。</p>
                        </div>
                        <button type="button" class="btn btn-success" data-dismiss="modal">取消删除</button>
                        <button id="TrueDelBtn" type="button" class="btn btn-danger" data-dismiss="modal" runat="server" onserverclick="TrueDelBtn_ServerClick">确认删除</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="Idhidden" runat="server" />

        <div id="SiderContent">
            <header id="demoheader">
                <h1>用户操作</h1>
            </header>
            <div id="demo-content" style="visibility: hidden">
                <div id="navigation">
                    <nav class="nav">
                        <ul>
                            <li><a id="EditProfile" href="#" runat="server">编辑个人资料&nbsp;&nbsp;&nbsp;<span name="editprofile"></span></a></li>
                            <li><a href="#" runat="server" id="LogOutBtn" onserverclick="LogOut">注销&nbsp;&nbsp;&nbsp;<span name="logoff"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
