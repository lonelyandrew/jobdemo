﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecruitCreate.aspx.cs" Inherits="JobDemo.Company.RecruitCreate" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Style/Company.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../UEditor/ueditor.config.js"></script>
    <script src="../UEditor/ueditor.all.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand" id="navhead">用人招聘发布</span>
        </div>
    </nav>
    <form id="ComRegisForm" runat="server" role="form" class="ComForm form-horizontal">
        <div class="form-group">
            <label for="AddRecTitle" class="col-sm-3 control-label">标题：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddRecTitle" placeholder="标题" runat="server" maxlength="40" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddRecPosition" class="col-sm-3 control-label">招聘职位：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddRecPosition" placeholder="招聘职位" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddRecCount" class="col-sm-3 control-label">数量：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddRecCount" placeholder="数量" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddRecPlace" class="col-sm-3 control-label">工作地点：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddRecPlaces" placeholder="工作地点" runat="server" maxlength="50" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddRecSalary" class="col-sm-3 control-label">薪资待遇：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddRecSalary" placeholder="薪资待遇" runat="server" maxlength="50" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddRecMajor" class="col-sm-3 control-label">专业限定：</label>
            <div class="col-sm-7">
                <select class="form-control" id="MajorSort" runat="server">
                </select>
            </div>
        </div>
        <div class="editor-field">
            <script id="container" name="content" type="text/plain" style="width: 600px; height: 200px;">
                详细情况
            </script>
            <script type="text/javascript">
                var ue = UE.getEditor('container');
            </script>
        </div>
        <div class="CompRegBtn form-group">
            <a class="btn btn-success compregbtn" runat="server" id="CompRecSubmit" onserverclick="RecCreateSubmit">
                <span class="glyphicon glyphicon-ok"></span>提交
            </a>
            <a class="btn btn-danger compregbtn" runat="server" id="ComprecCancel">
                <span class="glyphicon glyphicon-remove"></span>取消
            </a>
        </div>

    </form>

    <%--        Modal part--%>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog" id="kkk">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">用人招聘发布</h4>
                </div>
                <div class="modal-body">
                    <div class="jumbotron">
                        <h1>"发布尚未成功，确认退出？"</h1>
                        <p>——&nbsp;&nbsp;群贤毕至，一揽天下霸业。</p>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal">取消退出</button>
                    <button id="TrueCancelBtn" type="button" class="btn btn-danger" data-dismiss="modal">确认退出</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="ContentSpace" runat="server" />
    <script src="../Scripts/Company/RecCreate.js"></script>
</body>
</html>
