﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyRegister.aspx.cs" Inherits="JobDemo.Company.CompanyRegister" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用人单位注册</title>
    <link href="../Style/Company.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../UEditor/ueditor.config.js"></script>
    <script src="../UEditor/ueditor.all.min.js"></script>

</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">用人单位注册</span>
        </div>
    </nav>
    <form id="ComRegisForm" runat="server" role="form" class="ComForm form-horizontal">
        <div class="form-group">
            <label for="AddCompName" class="col-sm-3 control-label">单位名称：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddCompName" placeholder="单位名称" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddCompSite" class="col-sm-3 control-label">所在地：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddCompSite" placeholder="所在地" runat="server" maxlength="80" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddCompanyContact" class="col-sm-3 control-label">联系电话：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddCompContact" placeholder="联系电话" runat="server" maxlength="50" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddCompEmail" class="col-sm-3 control-label">电子邮件：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddCompEmail" placeholder="电子邮件" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddCompUsrName" class="col-sm-3 control-label">用户名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddCompUsrName" placeholder="用户名" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddCompPsd" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="AddCompPsd" placeholder="密码" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddCompRePsd" class="col-sm-3 control-label">重复密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="AddCompRePsd" placeholder="重复密码" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="editor-field">
            <script id="container" name="content" type="text/plain" style="width: 600px; height: 200px;">
                单位简介
            </script>
            <script type="text/javascript">
                var ue = UE.getEditor('container');
            </script>
        </div>
        <div class="CompRegBtn form-group">
            <a class="btn btn-success compregbtn" runat="server" id="CompRegSubmit" onserverclick="CompRegSubmitEvent">
                <span class="glyphicon glyphicon-ok"></span>提交
            </a>
            <a class="btn btn-danger compregbtn" runat="server" id="CompregCancel">
                <span class="glyphicon glyphicon-remove"></span>取消
            </a>
        </div>
    </form>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog" id="kkk">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">注册账号，开启明天</h4>
                </div>
                <div class="modal-body">
                    <div class="jumbotron">
                        <h1>"注册尚未成功，确认退出？"</h1>
                        <p>——&nbsp;&nbsp;求贤纳士，助力企业腾飞。</p>
                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal">取消退出</button>
                    <button id="TrueCancelBtn" type="button" class="btn btn-danger" data-dismiss="modal">确认退出</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="ContentSpace" runat="server" />

    <script src="../Scripts/Company/ComReg.js"></script>
</body>
</html>
