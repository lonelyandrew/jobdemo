﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="JobDemo.Statistics" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>历年统计数据</title>
    <link href="../Style/Company.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="Sider/jquery.sidr.light.css" rel="stylesheet" />
    <link href="Style/Statistics.css" rel="stylesheet" />

    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="Sider/jquery.sidr.min.js"></script>
    <script src="Scripts/Front/Statistics.js"></script>
    <script src="Scripts/Chart.min.js"></script>
</head>
<body>
    <form runat="server">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <span class="navbar-brand">历年统计数据&nbsp;&nbsp;<span class="glyphicon glyphicon-stats"></span></span>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <div class="navbar-form navbar-left" role="search">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" id="SearchBox" style="width: 170px;" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" id="SearchBtn"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="nav navbar-nav navbar-right" id="RegBar" runat="server" visible="false">
                    <li><a name="RegisBtn" href="#">注册</a></li>
                    <li><a href="Company/CompanyLogIn.aspx">企业登录</a></li>
                    <li><a href="Graduate/GradLog.aspx">毕业生登录</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right" id="ListBar" runat="server" visible="false">
                    <li><a id="MyProfile" runat="server" class="btn btn-link" disabled="disabled"></a></li>
                    <li><a id="SiderBtn" href="#"><span class="glyphicon glyphicon-list listbtn"></span></a></li>
                </ul>

            </div>
        </nav>

        <div id="content">
            <div class="panel panel-primary col">
                <div class="panel-heading">
                    <h3 class="panel-title">用人单位招聘人数TOP3</h3>
                </div>
                <div class="panel-body">
                    <canvas id="myChart" width="400" height="400"></canvas>
                    <input type="hidden" id="JsonComp" runat="server" />
                </div>
            </div>
            <div class="panel panel-primary col">
                <div class="panel-heading">
                    <h3 class="panel-title">近五年就业需求对比</h3>
                </div>
                <div class="panel-body">
                    <canvas id="myChart2" width="400" height="400"></canvas>
                    <input id="Json5Year" runat="server" type="hidden" />
                </div>
            </div>
            <div class="panel panel-primary col">
                <div class="panel-heading">
                    <h3 class="panel-title">最热门专业TOP3</h3>
                </div>
                <div class="panel-body">
                    <canvas id="myChart3" width="400" height="400"></canvas>
                    <input id="JsonHot3" runat="server" type="hidden" />
                </div>
            </div>
            <div class="panel panel-primary col">
                <div class="panel-heading">
                    <h3 class="panel-title">最冷门专业TOP3</h3>
                </div>
                <div class="panel-body">
                    <canvas id="myChart4" width="400" height="400"></canvas>
                    <input id="JsonCold3" runat="server" type="hidden" />
                </div>
            </div>
        </div>

        <div id="SiderContent">
            <header id="demoheader">
                <h1>用户操作</h1>
            </header>
            <div id="demo-content">
                <div id="navigation">
                    <nav class="nav">
                        <ul>
                            <li><a id="EditProfile" runat="server" href="#">编辑个人资料&nbsp;&nbsp;&nbsp;<span name="editprofile"></span></a></li>
                            <li><a href="#" runat="server" id="LogOutBtn" onserverclick="LogOut">注销&nbsp;&nbsp;&nbsp;<span name="logoff"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>



        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="display: none;">
            <div class="modal-dialog" id="kkk">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">注册账号，开启明天</h4>
                    </div>
                    <div class="modal-body">
                        <div class="jumbotron">
                            <h1>"开启全新人生."</h1>
                            <p>——&nbsp;&nbsp;加入我们，与千万求职者挑战自己。</p>
                        </div>
                        <a href="Graduate/GradReg.aspx" class="btn btn-success">求职注册入口</a>
                        <a href="Company/CompanyRegister.aspx" class="btn btn-warning">招聘注册入口</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
