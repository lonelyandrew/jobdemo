﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobDemo
{
    public partial class CompDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Id"] == null || Session["Role"] == null)
            {
                RegBar.Visible = true;
            }
            else
            {
                ListBar.Visible = true;
            }

            if (!IsPostBack)
            {
                MajorBind();

                if (Request.QueryString["Id"] != null)
                {
                    ContentBind();
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }

                if (null != Session["Id"])
                {
                    SetProfile();
                }
            }
        }

        private void SetProfile()
        {
            DataRow Row = null;
            if (Session["Role"].ToString() == "Admin")
            {
                Model.Admin model = new Model.Admin();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLAdmin.GetInfoById(model);
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["UserName"] + "&nbsp;&nbsp;登录角色：管理员";
                EditProfile.HRef = "Admin/AdminReg.aspx";
            }
            else if (Session["Role"].ToString() == "Company")
            {
                Model.Company model = new Model.Company();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLCompany.GetInfoById(model).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：用人单位";
                EditProfile.HRef = "Company/CompanyRegister.aspx";
            }
            else if (Session["Role"].ToString() == "Graduate")
            {
                //Model.Graduate model = new Model.Graduate();
                //model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLGraduate.GetGraduateInfo(Session["Id"].ToString()).Rows[0];
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["Name"] + "&nbsp;&nbsp;登录角色：毕业生";
                EditProfile.HRef = "Graduate/GradReg.aspx";
            }
            else
            {
                Response.Redirect("../Default.aspx");
            }
        }

        protected void LogOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("/Default.aspx");
        }

        private void MajorBind()
        {
            MajorSort.DataSource = BLLMajor.GetAllMajorInfo();
            MajorSort.DataBind();
        }

        private void ContentBind()
        {
            if (Request.QueryString["Id"] == null)
            {
                Response.Redirect("/Default.aspx");
            }

            using (Model.Company model = new Model.Company())
            {
                model.Id = Int32.Parse(Request.QueryString["Id"].ToString());
                DataTable Table = BLLCompany.GetInfoById(model);

                if (Table == null)
                {
                    Response.Redirect("/Default.aspx");
                }
                else
                {
                    DetailContent.DataSource = Table;
                    DetailContent.DataBind();
                    CompContent.InnerHtml = Table.Rows[0]["Description"].ToString();
                }
            }
        }
    }
}