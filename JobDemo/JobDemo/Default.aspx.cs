﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobDemo
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MajorBind();
            }
        }

        private void MajorBind()
        {
            MajorSort.DataSource = BLLMajor.GetAllMajorInfo();
            MajorSort.DataBind();
        }
    }
}