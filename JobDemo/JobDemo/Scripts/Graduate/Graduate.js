﻿$(function () {

    myDate = new Date();

    var DateString = myDate.toLocaleDateString().replace("/", "-").replace("/", "-");

    $("#SexSwitch").bootstrapSwitch({
        onText: "男", offText: "女", offColor: 'success', onSwitchChange: function (event, state) {
            $("#AddGradSex").attr("checked", state);
        }
    });


    $("#SexSwitch").bootstrapSwitch("state", $("#AddGradSex").attr("checked"));

    $('#AddGrudTime').datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $('#AddGrudBirthday').datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $('#AddGrudTime').datetimepicker('setEndDate', DateString);
    $('#AddGrudBirthday').datetimepicker('setEndDate', DateString);

    $("#ShowBtnTime").click(function () {
        $('#AddGrudTime').datetimepicker('show');
    })

    $("#RemoveBtnTime").click(function () {
        $('#AddGrudTime').val("");
    })

    $("#ShowBtnBirthday").click(function () {
        $('#AddGrudBirthday').datetimepicker('show');
    })

    $("#RemoveBtnBirthday").click(function () {
        $('#AddGrudBirthday').val("");
    })

    //$("#AddGradName").tooltip({ placement: 'right', trigger: 'manual', title: 'kkkkkkkkk' }).tooltip("show");
    //$("#AddGradUsrName").tooltip();
    //$("#AddGradSchool").tooltip();

    $("#GradRegBtn").click(function () {
        var inputs = $("input[type=text]");

        var infostate = true;
        inputs.each(function (index, value) {

            if ($(this).val().length == 0) {
                $(this).tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
                $(this).parent().addClass("has-error");
                infostate = false;
                return false;
            }
            else {
                $(this).tooltip("hide")
                $(this).parent().removeClass("has-error").addClass("has-success");
            }
        })

        if (infostate) {
            var psds = $("input[type=password]");

            var psdstate = true;

            psds.each(function () {
                if ($(this).val().length == 0) {
                    $(this).tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
                    $(this).parent().addClass("has-error");
                    state = false;
                    return false;
                }
                else {
                    $(this).tooltip("hide")
                    $(this).parent().removeClass("has-error").addClass("has-success");
                }
            })

            if (psdstate) {
                if (psds.eq(0).val() != psds.eq(1).val()) {
                    psds.eq(1).tooltip('destroy');
                    psds.eq(1).tooltip({ placement: 'right', trigger: 'manual', title: '密码不一致！' }).tooltip('show');
                    psds.eq(1).parent().removeClass("has-success").addClass("has-error");
                    psds.eq(0).parent().removeClass("has-success").addClass("has-error");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        else {
            return false;
        }
    })

    $("#GrudregCancel").click(function () {
        $("#myModal").modal();
    })

    $("#TrueCancelBtn").click(function () {
        history.back();
        //window.location.href = "../Default.aspx";
    })

    //$("#SexSwitch").change(function () {
    //    $("#AddGradSex").attr("checked", $(this).attr("checked"));
    //    alert($("#AddGradSex").attr("checked"));
    //})


    $('#AddGradSex').on('switch-change', function (e, data) {
        alert(data.value);
    });
})