﻿$(function () {
    $("#GrudLogSubmit").click(function () {

        if ($("#LogGradUsrName").val().length == 0) {
            $("#LogGradUsrName").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#LogGradUsrName").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#LogGradUsrName").tooltip("hide");
            $("#LogGradUsrName").parent().addClass("has-success");
        }

        if ($("#LogCompPsd").val().length == 0) {
            $("#LogCompPsd").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#LogCompPsd").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#LogCompPsd").tooltip("hide")
            $("#LogCompPsd").parent().addClass("has-success");
        }

        return true;
    })

    $("#GradrLogCancel").click(function () {
        window.location.href = "../Default.aspx";
    })
})