﻿$(function () {
    $("#CompRegSubmit").click(function () {


        var inputs = $("input[type=text]");
        var infostate = true;
        inputs.each(function () {
            if ($(this).val().length == 0) {
                $(this).tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
                $(this).parent().removeClass("has-success").addClass("has-error");
                infostate = false;
                return false;
            }
            else {
                $(this).tooltip("hide")
                $(this).parent().removeClass("has-error").addClass("has-success");
            }
        })
        var psdsstate = true;

        if (infostate) {
            var psds = $("input[type=password]");

            psds.each(function () {
                if ($(this).val().length == 0) {
                    $(this).tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
                    $(this).parent().removeClass("has-success").addClass("has-error");
                    psdsstate = false;
                    return false;
                }
                else {
                    $(this).tooltip("hide")
                    $(this).parent().removeClass("has-error").addClass("has-success");
                }
            })
        }
        else {
            return false;
        }

        if (psdsstate) {

            if (psds.eq(0).val() != psds.eq(1).val()) {
                psds.eq(1).tooltip("destroy");
                psds.eq(1).tooltip({ placement: 'right', trigger: 'manual', title: '密码不一致！' }).tooltip("show");
                psds.eq(1).parent().removeClass("has-success").addClass("has-error");
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    })

    $("#TrueCancelBtn").click(function () {
        history.back();
        //window.location.href = "../Default.aspx";
    })

    $("#CompregCancel").click(function () {
        $("#myModal").modal();
    })

    $("#myModal").on("show.bs.modal", function (e) {
        $("#ComRegisForm").hide();

    })

    $("#myModal").on("hidden.bs.modal", function (e) {
        $("#ComRegisForm").show();
    })

    ue.ready(function () {
        ue.setContent($("#ContentSpace").val());
    });
})