﻿$(function () {
    $("#CompRecSubmit").click(function () {

        var inputs = $("input[type=text]");

        var infostate = true;

        inputs.each(function (index, value) {

            if ($(this).val().length == 0) {
                $(this).tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
                $(this).parent().removeClass("has-success").addClass("has-error");
                infostate = false;
                return false;
            }
            else {

                if (index == 2) {
                    if (isNaN($(this).val())) {
                        $(this).tooltip("destroy");
                        $(this).tooltip({ placement: 'right', trigger: 'manual', title: '请填写正确数字，数值在1-1000之间！' }).tooltip("show");
                        $(this).parent().removeClass("has-success").addClass("has-error");
                        infostate = false;
                        return false;
                    }
                    else if ($(this).val() < 1 || $(this).val() > 1000) {
                        $(this).tooltip("destroy");
                        $(this).tooltip({ placement: 'right', trigger: 'manual', title: '数值错误！' }).tooltip("show");
                        $(this).parent().removeClass("has-success").addClass("has-error");
                        infostate = false;
                        return false;
                    }
                    else {
                        $(this).tooltip("hide")
                        $(this).parent().removeClass("has-error").addClass("has-success");
                    }
                }

                $(this).tooltip("hide")
                $(this).parent().removeClass("has-error").addClass("has-success");
            }
        })

        return infostate;
    })

    $("#ComprecCancel").click(function () {
        $("#myModal").modal();
    })

    $("#TrueCancelBtn").click(function () {
        location.href = "RecManagement.aspx";
    })

    $("#myDelModal").on("show.bs.modal", function (e) {
        $("#hidespace").hide()
    })

    $("#myDelModal").on("hidden.bs.modal", function (e) {
        $("#Idhidden").val("");
        $("#hidespace").show();
    })

    ue.ready(function () {
        ue.setContent($("#ContentSpace").val());
    });

})