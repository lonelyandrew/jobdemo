﻿$(function () {
    $("#CompLogSubmit").click(function () {

        if ($("#LogCompUsrName").val().length == 0) {
            $("#LogCompUsrName").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#LogCompUsrName").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#LogCompUsrName").tooltip("hide");
            $("#LogCompUsrName").parent().addClass("has-success");
        }

        if ($("#LogAdminPsd").val().length == 0) {
            $("#LogAdminPsd").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#LogAdminPsd").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#LogAdminPsd").tooltip("hide")
            $("#LogAdminPsd").parent().addClass("has-success");
        }

        return true;
    })

    $("#CompregCancel").click(function () {
        location.href = "../Default.aspx";
    })
})