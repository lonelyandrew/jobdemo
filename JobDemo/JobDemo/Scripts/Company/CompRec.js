﻿$(function () {

    $("#CreateRecBtn").click(function () {
        location.href = "RecruitCreate.aspx";
    })

    $("[name=delrec]").click(function () {
        $("#Idhidden").val($(this).attr("value"));
        $("#myDelModal").modal();
    })

    $("#FailedTrigger").click(function () {
        $("FailedModal").modal();
    })

    $("#editrec").click(function () {
        location.href = "RecruitCreate.aspx?id=" + $(this).val();
    })

    $("#SiderBtn").sidr({
        name: 'sidr-existing-content',
        source: '#demoheader, #demo-content',
        side: 'right'
    });

    $("[name=logoff]").addClass("glyphicon glyphicon-off");
    $("[name=editprofile]").addClass("glyphicon glyphicon-user");

    
})