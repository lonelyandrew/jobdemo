﻿$(
    function () {


        $("#AddMajorSubmit").click(function () {
            if ($("#AddMajorName").val().length < 1) {
                alert("专业名称不得为空。");
                return false;
            }
            else if ($("#AddMajorName").val().length > 20) {
                alert("专业名称最大长度为20个字符。");
                return false;
            }
            else {
                return true;
            }
        })

        $("#MajorSelectAll").click(function () {
            if ($(this).attr("checked") == "checked") {
                $("input[type=checkbox]").attr("checked", "checked");
            }
            else {
                $("input[type=checkbox]").removeAttr("checked");
            }
        })

        $("input[name=MajorId]").click(function () {
            if ($(this).attr("checked") != "checked" && $("#MajorSelectAll").attr("checked") == "checked") {
                $("#MajorSelectAll").removeAttr("checked");
            }
        })

        $("a[name=delmajor]").click(function () {
            if (confirm("确认删除？")) {
                return true;
            }
            else {
                return false;
            }
        })

        $("#MultiDelBtn").click(function () {
            if (confirm("确认删除？")) {
                return true;
            }
            else {
                return false;
            }
        })

        $("[name=searchselect]").click(function () {
            $("#searchsortselect").html($(this).text() + "<b class='caret'>");
        })

        $("[name=navli]").click(function () {
            $("[name=navli]").removeClass("active");
            $(this).addClass("active");
        })

        $("#UploadBtn").click(function () {
            $('input[id=lefile]').click();
        })

        $('input[id=lefile]').change(function () {
            $('#UpFilePath').val($(this).val());
        });

        $("#SiderContent").hide();

        $("#SiderBtn").sidr({
            name: 'sidr-existing-content',
            source: '#demoheader, #demo-content',
            side: 'right'
        });

        $("[name=deloperation]").click(function () {
            $("#myDelModal").modal('show');
            $("#IdSpace").val($(this).attr("value"));
            $("#SortSpace").val($(this).attr("sort"));
        })

        $("#myDelModal").on("show.bs.modal", function (e) {
            $("#hiddencontent").hide()
        })

        $("#myDelModal").on("hidden.bs.modal", function (e) {
            $("#IdSpace").val("");
            $("#SortSpace").val("");
            $("#hiddencontent").show();
        })
    })