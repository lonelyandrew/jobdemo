﻿$(function () {
    $("#AdminLogSubmit").click(function () {

        if ($("#LogAdminUsrName").val().length == 0) {
            $("#LogAdminUsrName").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#LogAdminUsrName").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#LogAdminUsrName").tooltip("hide");
            $("#LogAdminUsrName").parent().addClass("has-success");
        }

        if ($("#LogAdminPsd").val().length == 0) {
            $("#LogAdminPsd").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#LogAdminPsd").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#LogAdminPsd").tooltip("hide")
            $("#LogAdminPsd").parent().addClass("has-success");
        }

        return true;
    })
})