﻿$(function () {

    $("#SiderBtn").sidr({
        name: 'sidr-existing-content',
        source: '#demoheader, #demo-content',
        side: 'right'
    });

    $("#SearchBtn").click(function () {
        if ($("#SearchBox").val().length == 0) {
            $("#SearchBox").tooltip({ placement: 'bottom', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#SearchBox").parent().addClass("has-error");

            timeoutID = window.setTimeout(DestroyTooltip, 2000);
            return false;
        }
        if ($("#FirstLi").text() == "专业信息") {
            location.href = "RecList.aspx?qw=" + $("#SearchBox").val();
        }
        else {
            location.href = "RecList.aspx?qw=" + $("#SearchBox").val() + "&mi=" + $("#FirstLi").attr("value");
        }

    })

    function DestroyTooltip() {
        $("#SearchBox").tooltip("destroy");
        $("#SearchBox").parent().removeClass("has-error");
    }

    $("[name=MajorName]").click(function () {
        $("#FirstLi").text($(this).text()).attr("value", $(this).attr("value"));
    })
})