﻿$(function () {

    $("#SiderBtn").sidr({
        name: 'sidr-existing-content',
        source: '#demoheader, #demo-content',
        side: 'right'
    });


    $("[name=logoff]").addClass("glyphicon glyphicon-off");
    $("[name=editprofile]").addClass("glyphicon glyphicon-user");

    $("[name=RegisBtn]").click(function () {
        $('#myModal').modal('show');
    })

    $("#myModal").on("show.bs.modal", function (e) {
        $("#content").hide();
    })

    $("#myModal").on("hidden.bs.modal", function (e) {
        $("#content").show();
    })

    $("#SearchBtn").click(function () {
        if ($("#SearchBox").val().length == 0) {
            $("#SearchBox").tooltip({ placement: 'bottom', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#SearchBox").parent().addClass("has-error");

            timeoutID = window.setTimeout(DestroyTooltip, 2000);
            return false;
        }
        location.href = "RecList.aspx?qw=" + $("#SearchBox").val();
    })

    function DestroyTooltip() {
        $("#SearchBox").tooltip("destroy");
        $("#SearchBox").parent().removeClass("has-error");
    }

    var ctx = $("#myChart").get(0).getContext("2d");
    var myNewChart = new Chart(ctx).Bar(JSON.parse($("#JsonComp").val()));
    ctx = $("#myChart2").get(0).getContext("2d");
    myNewChart = new Chart(ctx).Bar(JSON.parse($("#Json5Year").val()));
    ctx = $("#myChart3").get(0).getContext("2d");
    myNewChart = new Chart(ctx).Bar(JSON.parse($("#JsonHot3").val()));
    ctx = $("#myChart4").get(0).getContext("2d");
    myNewChart = new Chart(ctx).Bar(JSON.parse($("#JsonCold3").val()));

})