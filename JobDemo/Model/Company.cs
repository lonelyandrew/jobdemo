﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Company : IDisposable
    {
        /// <summary>
        /// 公司ID
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// 公司名字
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 公司地址
        /// </summary>

        public string Site
        {
            get;
            set;
        }

        /// <summary>
        /// 公司联系方式
        /// </summary>
        public string Contact
        {
            get;
            set;
        }

        /// <summary>
        /// 公司email
        /// </summary>
        public string Email
        {
            get;
            set;
        }
        /// <summary>
        /// 公司账户
        /// </summary>

        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// 公司登陆密码
        /// </summary>

        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// 公司简介
        /// </summary>
        public string Description
        {
            get;
            set;
        }

        public enum JudgeCompLog
        {
            Success, Empty, CompUserNameNotExist, PasswordWrong,
        }

        public void Dispose()
        {
        }
    }


}
