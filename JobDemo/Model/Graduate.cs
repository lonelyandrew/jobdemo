﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Graduate
    {
        /// <summary>
        /// 学生ID
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// 学生姓名
        /// </summary>


        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 学生账户
        /// </summary>

        public string UserName
        {
            get;
            set;
        }


        /// <summary>
        /// 登陆密码
        /// </summary>
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// 性别
        /// </summary>

        public string Sex
        {
            get;
            set;
        }

        /// <summary>
        /// 生日
        /// </summary>

        public string Birthday
        {
            get;
            set;
        }

        /// <summary>
        /// 出生地
        /// </summary>

        public string Location
        {
            get;
            set;
        }
        /// <summary>
        /// 毕业时间
        /// </summary>

        public string Time
        {
            get;
            set;
        }

        ///<summary>
        ///专业号
        ///</summary>
        public string MajorId
        {
            get;
            set;
        }

        ///<summary>
        ///毕业学校
        ///</summary>
        public string School
        {
            get;
            set;
        }

        public enum JudgeGraduateLog
        {
            Success, CompUserNameNotExist, PasswordWrong,
        }
    }
}
