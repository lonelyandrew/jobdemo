﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 用人需求类
    /// </summary>
    public class Recruitment
    {
        /// <summary>
        /// 用人需求Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用人单位Id
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// 专业要求Id
        /// </summary>
        public int MajorId { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 职位数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 工作地点 
        /// </summary>
        public string Place { get; set; }

        /// <summary>
        /// 工资薪酬
        /// </summary>
        public string Salary { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// 定时时间
        /// </summary>
        public string ReleaseTime { get; set; }

        /// <summary>
        /// 是否定时
        /// </summary>
        public bool Timed { get; set; }

        /// <summary>
        /// 照片职位
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// 招聘详情
        /// </summary>
        public string Content { get; set; }
    }
}
