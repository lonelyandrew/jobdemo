﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Major
    {
        /// <summary>
        /// 专业Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///  专业名称
        /// </summary>
        public string Name { get; set; }

        public enum MajorNameValid
        {
            Valid, OutofMaxLength, Empty,
        }
    }
}
