﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Model;
using System.Data;
using System.Web;

namespace BLL
{
    public static class BLLGraduate
    {
        public static bool AddGraduate(Graduate Model)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                DALObj.Model = Model;
                return DALObj.Add();
            }
        }

        public static bool DeleteGraduate(Graduate Model)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                DALObj.Model = Model;
                return DALObj.Delete();
            }
        }

        public static bool UpdateGraduate(Graduate Model)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                DALObj.Model = Model;
                return DALObj.Update();

            }
        }

        public static DataTable GetGraduateInfo()
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                return DALObj.GetList("");
            }
        }

        /// <summary>
        ///  判断用户名是否存在
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static bool ContainsGradUserName(String UserName)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                int a = DALObj.GetList(" UserName=N'" + UserName + "'").Rows.Count;
                if (a > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 获取学生Id
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static string GetGraduateId(Graduate Model)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                DALObj.Model = Model;
                DataTable GraduateTable = DALObj.GetList(" UserName = '" + Model.UserName + "'");
                if (GraduateTable.Rows.Count > 0)
                {
                    return GraduateTable.Rows[0]["Id"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 毕业生登陆错误判断
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static Model.Graduate.JudgeGraduateLog GraduateLog(Graduate Model)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                DataTable Table = DALObj.GetList(" UserName ='" + Model.UserName + "'");
                if (Table.Rows.Count == 0)
                {
                    return Graduate.JudgeGraduateLog.CompUserNameNotExist;
                }
                else
                {
                    if (Table.Rows[0]["Password"].ToString() != Model.Password)
                    {
                        return Graduate.JudgeGraduateLog.PasswordWrong;
                    }
                }
                return Graduate.JudgeGraduateLog.Success;

            }
        }

        /// <summary>
        /// 获取毕业生信息
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static DataTable GetGraduateInfo(string GradId)
        {
            using (DALGraduate DALObj = new DALGraduate())
            {
                if (GradId != "")
                {
                    return DALObj.GetList(" a.Id = " + GradId);
                }
                else
                {
                    return DALObj.GetList("");
                }

            }
        }
    }
}
