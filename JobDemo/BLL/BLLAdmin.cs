﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Model;
using System.Data;
using System.Web;


namespace BLL
{
    public class BLLAdmin
    {
        /// <summary>
        /// 添加管理员账户
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static bool AddAdmin(Admin Model)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                DALObj.Model = Model;
                return DALObj.Add();
            }
        }

        /// <summary>
        /// 删除管理员
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static bool DelAdmin(Admin Model)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                DALObj.Model = Model;
                return DALObj.Delete();
            }
        }

        /// <summary>
        ///管理员登陆错误 
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static Model.Admin.JudgeAdminLog JudgeAdminlog(Admin Model)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                DataTable Table = DALObj.GetList(" UserName='" + Model.UserName + "'");
                if (Table.Rows.Count == 0)
                {
                    return Admin.JudgeAdminLog.UserNameNotExist;
                }
                else
                {
                    if (Table.Rows[0]["Password"].ToString() != Model.Password)
                    {
                        return Admin.JudgeAdminLog.WrongPassword;
                    }
                }

                return Admin.JudgeAdminLog.Success;

            }

        }

        /// <summary>
        ///  判断管理员账号是否重复
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static bool ContainsAdminUserName(String UserName)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                int a = DALObj.GetList("UserName=N'" + UserName + "'").Rows.Count;
                if (a > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 获取管理员Id
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>s
        public static string GetAdminId(Admin Model)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                DALObj.Model = Model;
                DataTable CompanyTable = DALObj.GetList(" UserName = '" + Model.UserName + "'");
                if (CompanyTable.Rows.Count > 0)
                {
                    return CompanyTable.Rows[0]["Id"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        public static DataRow GetInfoById(Admin model)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                DALObj.Model = model;
                DataTable Table = DALObj.GetList(" Id = " + model.Id);
                if (Table.Rows.Count > 0)
                {
                    return Table.Rows[0];
                }
                else
                {
                    return null;
                }
            }
        }


        public static bool UpdateAdmin(Admin Model)
        {
            using (DALAdmin DALObj = new DALAdmin())
            {
                DALObj.Model = Model;
                return DALObj.Update();
            }
        }
    }
}
