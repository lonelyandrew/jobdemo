﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Model;
using System.Data;
using System.Web;

namespace BLL
{
    public static class BLLCompany
    {
        public static bool AddCompany(Company Model)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DALObj.Model = Model;
                return DALObj.Add();
            }
        }

        public static bool DeleteCompany(Company Model)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DALObj.Model = Model;
                return DALObj.Delete();
            }
        }

        public static bool UpdateCompany(Company Model)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DALObj.Model = Model;
                return DALObj.Update();
            }
        }

        public static DataTable GetCompInfo()
        {
            using (DALCompany DALObj = new DALCompany())
            {
                return DALObj.GetList("");
            }
        }
        /// <summary>
        /// 登陆错误判断
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static Model.Company.JudgeCompLog CompLogIn(Company Model)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DataTable Table = DALObj.GetList(" UserName ='" + Model.UserName + "'");
                if (Table.Rows.Count == 0)
                {
                    return Company.JudgeCompLog.CompUserNameNotExist;

                }
                else
                {
                    if (Table.Rows[0]["Password"].ToString() != Model.Password)
                    {
                        return Company.JudgeCompLog.PasswordWrong;
                    }
                }
                return Company.JudgeCompLog.Success;

            }


        }
        /// <summary>
        /// 判断公司名字是否存在
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>

        public static bool ContainsCompName(String Name)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                int a = DALObj.GetList("Name=N'" + Name + "'").Rows.Count;
                if (a > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        ///  判断公司账号是否重复
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static bool ContainsCompUserName(String UserName)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                int a = DALObj.GetList("UserName=N'" + UserName + "'").Rows.Count;
                if (a > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 获取公司Id
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>s
        public static string GetCompanyId(Company Model)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DALObj.Model = Model;
                DataTable CompanyTable = DALObj.GetList(" UserName = '" + Model.UserName + "'");
                if (CompanyTable.Rows.Count > 0)
                {
                    return CompanyTable.Rows[0]["Id"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        public static DataTable GetInfoById(Company model)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DALObj.Model = model;
                DataTable Table = DALObj.GetList(" Id = " + model.Id);
                if (Table.Rows.Count > 0)
                {
                    return Table;
                }
                else
                {
                    return null;
                }
            }
        }

        public static DataRow GetCompInfo(string Id)
        {
            using (DALCompany DALObj = new DALCompany())
            {
                DataTable Table = DALObj.GetList(" Id = " + Id);
                if (Table.Rows.Count > 0)
                {
                    return Table.Rows[0];
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
