﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Tools
{
    public static class ScriptHelper
    {
        public static void Alert(string Information,Page RequestPage)
        {
            RequestPage.ClientScript.RegisterStartupScript(RequestPage.GetType(), "", "<script>alert('"+Information+"')</script>");
        }
    }
}
