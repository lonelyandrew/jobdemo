﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlHelper;
using System.Data.SqlClient;

namespace TestLib
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectionPoolManager PoolManager = new ConnectionPoolManager(2);

            SqlConnection Conn = PoolManager.GetConnection();

            SqlConnection con = PoolManager.GetConnection();

            //PoolManager.CloseConnection(Conn);

            //SqlConnection co = PoolManager.GetConnection();

            //if (PoolManager.CloseConnection(Conn))
            //{
            //    Console.WriteLine("Success!");
            //}
            //if (PoolManager.CloseConnection(con))
            //{
            //    Console.WriteLine("Success!");
            //}

            PoolManager.DisposeAllConnection();

            Console.WriteLine("Max:" + PoolManager.MaxConnectionCount + "|Open:" + PoolManager.OpeningConnectionCount + "|Active:" + PoolManager.ActiveConnectionCount);

            Console.ReadKey();
        }
    }
}
