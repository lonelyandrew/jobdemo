﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace SqlHelper
{
    public class ConnectionPoolManager
    {
        /// <summary>
        /// 最大连接数
        /// </summary>
        private int MaxConnectioncount;

        public int MaxConnectionCount
        {
            get
            {
                return MaxConnectioncount;
            }
        }

        /// <summary>
        /// 连接池激活连接连接数
        /// </summary>
        public int ActiveConnectionCount
        {
            get
            {
                return (UnlockedConnectionPool.Count + LockedConnectionPool.Count);
            }
        }

        /// <summary>
        /// 连接池打开连接连接数
        /// </summary>
        public int OpeningConnectionCount
        {
            get
            {
                return LockedConnectionPool.Count;
            }
        }

        /// <summary>
        /// 已锁定连接池
        /// </summary>
        private Dictionary<string, SqlConnection> LockedConnectionPool;

        /// <summary>
        /// 未锁定连接池
        /// </summary>
        private Dictionary<string, SqlConnection> UnlockedConnectionPool;

        public ConnectionPoolManager(int MaxConnectionCountInit)
        {
            if (MaxConnectionCountInit > 0)
            {
                MaxConnectioncount = MaxConnectionCountInit;
                UnlockedConnectionPool = new Dictionary<string, SqlConnection>();
                LockedConnectionPool = new Dictionary<string, SqlConnection>();
            }
            else
            {
                throw new Exception("Invalid initial MaxConnectionCount.");
            }
        }


        /// <summary>
        /// 判断是否可以申请新的连接(ReadOnly)
        /// </summary>
        private bool CanGetConnection
        {
            get
            {
                lock (this)
                {
                    if (LockedConnectionPool.Count < MaxConnectionCount)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// 从连接池中获取连接
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetConnection()
        {
            lock (this)
            {
                if (CanGetConnection)
                {
                    if (UnlockedConnectionPool.Count < 1)
                    {
                        while (true)
                        {
                            if (AddConnectionToPool())
                            {
                                break;
                            }
                        }
                    }

                    KeyValuePair<string, SqlConnection> SelectedPair = UnlockedConnectionPool.FirstOrDefault();
                    LockedConnectionPool.Add(GetValidKey(), SelectedPair.Value);
                    UnlockedConnectionPool.Remove(SelectedPair.Key);
                    return SelectedPair.Value;
                }
                else
                {
                    throw new Exception("Connection pool is filled.");
                }
            }
        }

        /// <summary>
        /// 向连接池中添加新的连接
        /// </summary>
        /// <returns></returns>
        private bool AddConnectionToPool()
        {
            try
            {
                SqlConnection NewConnection = new SqlConnection();
                UnlockedConnectionPool.Add(GetValidKey(), NewConnection);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 获取一个合法的键值
        /// </summary>
        /// <returns></returns>
        private string GetValidKey()
        {
            Random random = new Random();
            string NewKey = random.Next().ToString();
            while (LockedConnectionPool.ContainsKey(NewKey) || UnlockedConnectionPool.ContainsKey(NewKey))
            {
                NewKey = random.Next().ToString();
            }
            return NewKey;
        }


        /// <summary>
        /// 关闭当前数据库连接
        /// </summary>
        /// <param name="CurrentConnection"></param>
        /// <returns></returns>
        public bool CloseConnection(SqlConnection CurrentConnection)
        {
            lock (this)
            {
                if (CurrentConnection != null)
                {
                    if (LockedConnectionPool.ContainsValue(CurrentConnection))
                    {
                        try
                        {
                            UnlockedConnectionPool.Add(GetValidKey(), CurrentConnection);

                            foreach (KeyValuePair<string, SqlConnection> Item in LockedConnectionPool)
                            {
                                if (Item.Value == CurrentConnection)
                                {
                                    LockedConnectionPool.Remove(Item.Key);
                                    CurrentConnection.Close();
                                    return true;
                                }
                            }
                            return false;
                        }
                        catch
                        {
                            return false;
                        }
                    }
                    else
                    {
                        throw new Exception("Given SqlConnection isn't in the ConnectionPool.");
                    }

                }
                else
                {
                    throw new Exception("Can't close an null SqlConnection.");
                }
            }
        }

        /// <summary>
        /// 释放连接池中的所有连接
        /// </summary>
        /// <returns></returns>
        public bool DisposeAllConnection()
        {
            try
            {
                lock (this)
                {
                    foreach (KeyValuePair<string, SqlConnection> Item in LockedConnectionPool)
                    {
                        Item.Value.Close();
                        Item.Value.Dispose();
                    }

                    foreach (KeyValuePair<string, SqlConnection> Item in UnlockedConnectionPool)
                    {
                        Item.Value.Dispose();
                    }

                    LockedConnectionPool.Clear();
                    UnlockedConnectionPool.Clear();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 释放当前数据库连接
        /// </summary>
        /// <param name="CurrentConnection"></param>
        /// <returns></returns>
        public bool DisposeConnection(SqlConnection CurrentConnection)
        {
            try
            {
                lock (this)
                {
                    if (UnlockedConnectionPool.ContainsValue(CurrentConnection))
                    {
                        foreach (KeyValuePair<string, SqlConnection> Item in UnlockedConnectionPool)
                        {
                            if (Item.Value == CurrentConnection)
                            {
                                UnlockedConnectionPool.Remove(Item.Key);
                                CurrentConnection.Dispose();
                                return true;
                            }
                        }

                        return false;
                    }
                    else
                    {
                        if (LockedConnectionPool.ContainsValue(CurrentConnection))
                        {
                            foreach (KeyValuePair<string, SqlConnection> Item in LockedConnectionPool)
                            {
                                if (Item.Value == CurrentConnection)
                                {
                                    LockedConnectionPool.Remove(Item.Key);
                                    CurrentConnection.Dispose();
                                    return true;
                                }
                            }
                        }
                    }
                    throw new Exception("Given SqlConnection isn't in the ConnectionPool.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 关闭连接池中的所有连接
        /// </summary>
        /// <returns></returns>
        public bool CloseAllConnection()
        {
            try
            {
                lock (this)
                {
                    foreach (KeyValuePair<string, SqlConnection> Item in LockedConnectionPool)
                    {
                        Item.Value.Close();
                        UnlockedConnectionPool.Add(GetValidKey(), Item.Value);
                        LockedConnectionPool.Clear();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }
    }
}
