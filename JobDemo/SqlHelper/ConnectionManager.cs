﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SqlHelper
{
    public class ConnectionManager
    {
        /// <summary>
        /// 当前数据库连接
        /// </summary>
        private SqlConnection CurrentConnection;

        /// <summary>
        /// 当前数据可连接池
        /// </summary>
        private ConnectionPoolManager CurrentPoolManager = (ConnectionPoolManager)GlobalObj.GlobalObj.ApplicationStorage["ApplicationPoolManager"];

        /// <summary>
        /// 获取当前用户的数据库连接对象
        /// </summary>
        /// <returns></returns>
        private SqlConnection GetConnection()
        {
            if (CurrentConnection == null)
            {
                CurrentConnection = CurrentPoolManager.GetConnection();
            }
            return CurrentConnection;
        }

        /// <summary>
        /// 获取当前配置文件中的连接字符串
        /// </summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            string ConnectionString = CurrentConnection.ConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();
            if (String.IsNullOrEmpty(ConnectionString))
            {
                ConnectionString = null;
            }
            return ConnectionString;
        }

        /// <summary>
        /// 打开当前数据库连接
        /// </summary>
        private void OpenConnection()
        {
            try
            {
                if (CurrentConnection == null)
                {
                    CurrentConnection = CurrentPoolManager.GetConnection();
                }
                if (CurrentConnection.State != System.Data.ConnectionState.Broken && CurrentConnection.State != System.Data.ConnectionState.Closed)//TODO:#1
                {
                    return;
                }

                CurrentConnection.ConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();
                CurrentConnection.Open();
            }
            catch
            {
                throw new Exception("Failed to Open the Connection Of Database.");
            }
        }

        /// <summary>
        /// 关闭当前数据库连接
        /// </summary>
        private void CloseConnection()
        {
            if (CurrentConnection.State != System.Data.ConnectionState.Closed)//TODO:#2
            {
                CurrentPoolManager.CloseConnection(CurrentConnection);
            }
        }

        /// <summary>
        /// 释放当前数据库连接
        /// </summary>
        private void DisposeConnection()
        {
            if (CurrentConnection != null)
            {
                CurrentPoolManager.DisposeConnection(CurrentConnection);
                CurrentConnection = null;
            }
        }

        /// <summary>
        /// 执行无查询命令(多命令)
        /// </summary>
        /// <param name="Commands">命令数组</param>
        /// <returns></returns>
        public bool ExcuteNonQuery(SqlCommand[] Commands)
        {
            OpenConnection();
            SqlCommand Command = new SqlCommand();
            Command.Connection = CurrentConnection;
            SqlTransaction Transaction = CurrentConnection.BeginTransaction();
            Command.Transaction = Transaction;

            try
            {
                foreach (SqlCommand Item in Commands)
                {
                    if (Item != null)
                    {
                        Command.CommandText = Item.CommandText;
                        foreach (SqlParameter Parameter in Item.Parameters)
                        {
                            Command.Parameters.Add(new SqlParameter(Parameter.ParameterName, Parameter.Value));
                        }
                        Command.ExecuteNonQuery();
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                Transaction.Commit();
                return true;
            }
            catch
            {
                Transaction.Rollback();
                return false;
            }
            finally
            {
                Command.Dispose();
                CloseConnection();
            }
        }

        /// <summary>
        /// 执行无查询命令(单命令)
        /// </summary>
        /// <param name="Command">命令对象</param>
        /// <returns></returns>
        public bool ExcuteNonQuery(SqlCommand Command)
        {
            OpenConnection();
            Command.Connection = CurrentConnection;
            SqlTransaction Transaction = CurrentConnection.BeginTransaction();
            Command.Transaction = Transaction;
            try
            {

                if (Command.ExecuteNonQuery() > 0)
                {
                    Transaction.Commit();
                    return true;
                }
                else
                {
                    Transaction.Rollback();
                    return false;
                }

            }
            catch
            {
                Transaction.Rollback();
                return false;
            }
            finally
            {
                Command.Dispose();
                CloseConnection();
            }
        }

        /// <summary>
        /// 执行查询命令
        /// </summary>
        /// <param name="Command">查询命令</param>
        /// <returns></returns>
        public DataTable ExcuteDataTable(SqlCommand Command)
        {
            try
            {
                if (Command != null)
                {
                    OpenConnection();
                    Command.Connection = CurrentConnection;
                    SqlDataReader DataReader = Command.ExecuteReader();
                    DataTable Table = new DataTable();
                    Table.Load(DataReader);
                    return Table;
                }
                else
                {
                    return null;
                }
            }
            catch
            {

                throw new Exception();
            }
            finally
            {
                CloseConnection();
                Command.Dispose();
            }

        }
    }
}
