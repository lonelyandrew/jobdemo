﻿using Model;
using IDAL;
using SqlHelper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public  class DALAdmin:IAdmin ,IDisposable 
    {
        public Admin Model = new Admin();
        public ConnectionManager SqlConnection = new ConnectionManager();

        /// <summary>
        /// 添加管理员账户
        /// </summary>
        /// <returns></returns>
        public bool Add()
        {
            if (!String.IsNullOrWhiteSpace(Model.UserName) && !String.IsNullOrWhiteSpace(Model.Password))
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("INSERT INTO Admin(UserName,Password) VALUES(@AUserName,@APassword) ");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@AUserName", Model.UserName));
                Command.Parameters.Add(new SqlParameter("@APassword", Model.Password));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除管理员账户
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("DELETE FROM Admin WHERE Id = @AdminId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@AdminId", Model.Id));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取管理员账户信息
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        public DataTable GetList(string Where)
        {
            StringBuilder CommandText = new StringBuilder();
            CommandText.Append("SELECT Id,UserName,Password FROM Admin");
            if (Where != "")
            {
                CommandText.Append(" WHERE " + Where);
            }
            SqlCommand Command = new SqlCommand();
            Command.CommandText = CommandText.ToString();
            return SqlConnection.ExcuteDataTable(Command);
        }


        public bool Update()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("UPDATE Admin SET UserName=@AUserName,Password=@APassword");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@AUserName", Model.UserName));
                Command.Parameters.Add(new SqlParameter("@APassword", Model.Password ));
                return (SqlConnection.ExcuteNonQuery(Command));

            }
            else
            {
                return false;
            }
           // throw new NotImplementedException();
        }

        public void Dispose()
        {
           // throw new NotImplementedException();
        }
    }
}
