﻿using IDAL;
using Model;
using SqlHelper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class DALGraduate : IGraduate, IDisposable
    {
        public Graduate Model = new Graduate();
        public ConnectionManager SqlConnection = new ConnectionManager();
        /// <summary>
        /// 添加学生账户
        /// </summary>
        /// <returns></returns>
        public bool Add()
        {
            if (!String.IsNullOrWhiteSpace(Model.Name) && !String.IsNullOrWhiteSpace(Model.UserName) && !String.IsNullOrWhiteSpace(Model.Password) && !String.IsNullOrWhiteSpace(Model.Time) && !String.IsNullOrWhiteSpace(Model.School) && !String.IsNullOrWhiteSpace(Model.MajorId) && !String.IsNullOrWhiteSpace(Model.Sex))
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("INSERT INTO Graduate(Name,UserName,Password,Sex,Location,Time,Birthday,MajorId,School) VALUES(@GName,@GUserName,@GPassword,@GSex,@GLocation,@GTime,@GBirthday,@GMajorId,@GSchool)");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@GName", Model.Name));
                Command.Parameters.Add(new SqlParameter("@GUserName", Model.UserName));
                Command.Parameters.Add(new SqlParameter("@GPassword", Model.Password));
                Command.Parameters.Add(new SqlParameter("@GSex", Model.Sex));
                Command.Parameters.Add(new SqlParameter("@GLocation", Model.Location));
                Command.Parameters.Add(new SqlParameter("@GTime", Model.Time));
                Command.Parameters.Add(new SqlParameter("@GBirthday", Model.Birthday));
                Command.Parameters.Add(new SqlParameter("@GMajorId", Model.MajorId));
                Command.Parameters.Add(new SqlParameter("@GSchool", Model.School));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///删除学生信息
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("DELETE FROM Graduate WHERE Id = @GraduateId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("GraduateId", Model.Id));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("UPDATE Graduate SET Name=@GName,MajorId=@GMajorId,UserName=@GUserName,Password=@GPassword,");
                CommandText.Append("Location = @GLocation,Time=@GTime,Birthday=@GBirthday,School=@GSchool,Sex = @GSex WHERE Id=@GraduateId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@GName", Model.Name));
                Command.Parameters.Add(new SqlParameter("@GUserName", Model.UserName));
                Command.Parameters.Add(new SqlParameter("@GPassword", Model.Password));
                Command.Parameters.Add(new SqlParameter("@GLocation", Model.Location));
                Command.Parameters.Add(new SqlParameter("@GTime", Model.Time));
                Command.Parameters.Add(new SqlParameter("@GBirthday", Model.Birthday));
                Command.Parameters.Add(new SqlParameter("@GMajorId", Model.MajorId));
                Command.Parameters.Add(new SqlParameter("@GSchool", Model.School));
                Command.Parameters.Add(new SqlParameter("@GraduateId", Model.Id));
                Command.Parameters.Add(new SqlParameter("@GSex", Model.Sex));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }

        }

        public void Dispose()
        {
            // throw new NotImplementedException();
        }

        /// <summary>
        /// 获取学生信息
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        public DataTable GetList(string Where)
        {
            StringBuilder CommandText = new StringBuilder();

            CommandText.Append("SELECT a.Id,a.Name,UserName,Password,Sex,Location,Time,Birthday,MajorId,School,b.Name AS MajorName FROM Graduate a ,Major b WHERE a.Majorid = b.Id ");
            if (Where != "")
            {
                CommandText.Append("AND " + Where);
            }
            SqlCommand Command = new SqlCommand();
            Command.CommandText = CommandText.ToString();
            return SqlConnection.ExcuteDataTable(Command);

        }
    }
}
