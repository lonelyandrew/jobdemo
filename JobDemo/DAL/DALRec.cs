﻿using IDAL;
using Model;
using SqlHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALRec : IRec, IDisposable
    {

        public Recruitment Model = new Recruitment();

        public ConnectionManager SQLConnection = new ConnectionManager();

        public bool Add()
        {
            if (!String.IsNullOrWhiteSpace(Model.Name))
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("INSERT INTO Recruitment (Name,Count,Place,Salary,MajorId,CompanyId,Timed,Time,ReleaseTime,Content,Position)");
                CommandText.Append("VALUES(@RecName,@RecCount,@RecPlace,@RecSalary,@MajorId,@CompanyId,@RecTimed,@RecTime,@RecRTime,@RecContent,@RecPosition)");

                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("RecName", Model.Name));
                Command.Parameters.Add(new SqlParameter("RecCount", Model.Count));
                Command.Parameters.Add(new SqlParameter("RecPlace", Model.Place));
                Command.Parameters.Add(new SqlParameter("RecSalary", Model.Salary));
                Command.Parameters.Add(new SqlParameter("MajorId", Model.MajorId));
                Command.Parameters.Add(new SqlParameter("CompanyId", Model.CompanyId));
                Command.Parameters.Add(new SqlParameter("RecTimed", Model.Timed));
                Command.Parameters.Add(new SqlParameter("RecTime", Model.Time));
                Command.Parameters.Add(new SqlParameter("RecRTime", Model.ReleaseTime));
                Command.Parameters.Add(new SqlParameter("RecContent", Model.Content));
                Command.Parameters.Add(new SqlParameter("RecPosition", Model.Position));

                return (SQLConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        public bool Delete()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("DELETE FROM Recruitment WHERE Id = @RecId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("RecId", Model.Id));
                return (SQLConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("UPDATE Recruitment SET Name = @RecName,Count = @RecCount,Place = @RecPlace,Salary = @RecSalary,MajorId = @RecMajorId,Content = @RecContent,Position = @RecPosition WHERE Id = @RecId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("RecName", Model.Name));
                Command.Parameters.Add(new SqlParameter("RecCount", Model.Count));
                Command.Parameters.Add(new SqlParameter("RecPlace", Model.Place));
                Command.Parameters.Add(new SqlParameter("RecSalary", Model.Salary));
                Command.Parameters.Add(new SqlParameter("RecMajorId", Model.MajorId));
                Command.Parameters.Add(new SqlParameter("RecContent", Model.Content));
                Command.Parameters.Add(new SqlParameter("RecPosition", Model.Position));
                Command.Parameters.Add(new SqlParameter("RecId", Model.Id));
                return (SQLConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        public DataTable GetList(string Where)
        {
            StringBuilder CommandText = new StringBuilder();
            CommandText.Append("SELECT a.Id,a.Name,a.Count,a.Place,a.Salary,a.Time,a.Content,a.Position,b.Name AS MajorName,c.Name AS CompanyName,c.Id AS CompanyId FROM Recruitment a,Major b,Company c WHERE a.MajorId = b.Id AND a.CompanyId = c.Id ");
            if (Where != "")
            {
                CommandText.Append("AND" + Where);
                CommandText.Append(" ORDER BY a.Id DESC");
            }
            SqlCommand Command = new SqlCommand();
            Command.CommandText = CommandText.ToString();
            return SQLConnection.ExcuteDataTable(Command);
        }

        public DataTable GetEdit(string Where)
        {
            StringBuilder CommandText = new StringBuilder();
            CommandText.Append("SELECT Id,Name,Count,Place,Salary,Content,Position,MajorId FROM Recruitment ");
            if (Where != "")
            {
                CommandText.Append(" WHERE " + Where);
            }
            SqlCommand Command = new SqlCommand();
            Command.CommandText = CommandText.ToString();
            return SQLConnection.ExcuteDataTable(Command);
        }

        public DataTable GetNeedList(string strWhere)
        {
            try
            {
                StringBuilder stringbuilder = new StringBuilder();
                stringbuilder.Append(" SELECT  f.RecruitmentID AS Id,f.RecruitmentName AS Name,f.Time AS Time FROM ( ");
                stringbuilder.Append(" SELECT e.CompanyName ,e.MajorName ,e.Place ,e.Position ,e.mid,e.RecruitmentID,e.RecruitmentName,e.Time FROM ( ");
                stringbuilder.Append(" SELECT c.CompanyId,c.mid,c.Position,c.Place,c.MajorName,c.RecruitmentName,c.RecruitmentID,d.Id,d.Name AS CompanyName,c.Time FROM (   ");
                stringbuilder.Append(" SELECT a.Id AS mid,a.Name AS MajorName,b.CompanyId,b.Position,b.Place,b.MajorId,b.Name AS RecruitmentName,b.Id As RecruitmentID,b.Time FROM  ");
                stringbuilder.Append(" Major a INNER JOIN Recruitment b ON a.Id=b.MajorId)c   INNER JOIN Company d ON c.CompanyId=d.Id) e )f ");
                stringbuilder.Append(" WHERE " + strWhere);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = stringbuilder.ToString();
                return SQLConnection.ExcuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTop5PeopleCountList()
        {
            try
            {
                StringBuilder StrBuilder = new StringBuilder();
                StrBuilder.Append(" SELECT top 5 CompanyId,Company.Name,SUM(Count) AS CompReq ");
                StrBuilder.Append(" FROM Recruitment,Company WHERE  Company.Id=Recruitment.CompanyId GROUP BY CompanyId,Company.Name  ");
                StrBuilder.Append(" ORDER BY SUM(Count) DESC ");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = StrBuilder.ToString();
                return SQLConnection.ExcuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTop3PeopleCountList()
        {
            try
            {
                string Time = DateTime.Now.Year.ToString();
                StringBuilder StrBuilder = new StringBuilder();
                //StrBuilder.Append(" SELECT top 3 Major.Name,SUM(Count) FROM Recruitment ,Major  WHERE Time LIKE '%" + Time + "%'  ");
                //StrBuilder.Append(" and Recruitment.MajorId=Major.Id  GROUP BY MajorId,Major.Name   ORDER BY SUM(Count) DESC ");
                StrBuilder.Append(" SELECT top 3 Major.Name AS MajorName,SUM(Count) AS MajorPeopleCount FROM Recruitment ,Major ,Company ");
                StrBuilder.Append(" WHERE (CONVERT(int,SUBSTRING(Time,1,4)) BETWEEN " + Time + "-4 AND " + Time + ") AND Recruitment.MajorId=Major.Id AND Recruitment.CompanyId=Company.Id  ");
                StrBuilder.Append(" GROUP BY MajorId,Major.Name   ORDER BY SUM(Count) DESC ");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = StrBuilder.ToString();
                return SQLConnection.ExcuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetBottom3PeopleCountList()
        {
            try
            {
                string Time = DateTime.Now.Year.ToString();
                StringBuilder StrBuilder = new StringBuilder();
                StrBuilder.Append(" SELECT top 3 Major.Name AS MajorName,SUM(Count) AS MajorPeopleCount FROM Recruitment ,Major ,Company ");
                StrBuilder.Append(" WHERE (CONVERT(int,SUBSTRING(Time,1,4)) BETWEEN " + Time + "-4 AND " + Time + ") AND Recruitment.MajorId=Major.Id AND Recruitment.CompanyId=Company.Id  ");
                StrBuilder.Append(" GROUP BY MajorId,Major.Name   ORDER BY SUM(Count) ASC ");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = StrBuilder.ToString();
                return SQLConnection.ExcuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetYearSumPeopleCountList()
        {
            try
            {
                string Time = DateTime.Now.Year.ToString();
                StringBuilder StrBuilder = new StringBuilder();
                StrBuilder.Append(" SELECT CONVERT(int,SUBSTRING(Time,1,4)) AS YearTime ,SUM(Count) AS MaxCount FROM Recruitment AS a ,Major AS b,Company AS c ");
                StrBuilder.Append("WHERE( CONVERT(int,SUBSTRING(Time,1,4))  BETWEEN " + Time + "-4 AND " + Time + ") AND a.MajorId = b.Id AND a.CompanyId = c.Id ");
                StrBuilder.Append("GROUP BY CONVERT(int,SUBSTRING(Time,1,4))");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = StrBuilder.ToString();
                return SQLConnection.ExcuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //throw new NotImplementedException();
        }

        public void Dispose()
        {
        }
    }
}
