﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDAL
{
    /// <summary>
    /// DAL层公共接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICommon
    {
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model">模型对象</param>
        /// <returns></returns>
        bool Add();

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model">模型对象</param>
        /// <returns></returns>
        bool Delete();

        /// <summary>
        /// 更新对象
        /// </summary>
        /// <param name="model">模型对象</param>
        /// <returns></returns>
        bool Update();

        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="Where">查询条件</param>
        /// <returns></returns>
        DataTable GetList(string Where);
    }
}
